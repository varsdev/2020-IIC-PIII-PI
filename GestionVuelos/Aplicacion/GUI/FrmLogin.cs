﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Aplicacion.BOL;
using Aplicacion.Entities;

namespace Aplicacion.GUI
{
    public partial class FrmLogin : Form
    {

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        public FrmLogin()
        {
            InitializeComponent();
            CenterToScreen();
        }      

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            if (txtCedula.Text == "Cedula")
            {
                txtCedula.Text = "";
                txtCedula.ForeColor = Color.LightGray;
                lblMensaje.Visible = false;
            }
        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            if (txtCedula.Text == "")
            {
                txtCedula.Text = "Cedula";
                txtCedula.ForeColor = Color.DimGray;
            }
        }

        private void txtContra_Enter(object sender, EventArgs e)
        {
            if (txtContra.Text == "Contraseña")
            {
                txtContra.Text = "";
                txtContra.ForeColor = Color.LightGray;
                txtContra.UseSystemPasswordChar = true;
                lblMensaje.Visible = false;
            }
        }

        private void txtContra_Leave(object sender, EventArgs e)
        {
            if (txtContra.Text == "")
            {
                txtContra.Text = "Contraseña";
                txtContra.ForeColor = Color.DimGray;
                txtContra.UseSystemPasswordChar = false;
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void FrmLogin_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtCedula.Text != "Cedula")
            {
                if (txtContra.Text != "Contraseña")
                {
                    Usuario u = new Usuario();
                    u.cedula = txtCedula.Text.Trim();
                    u.contrasenna = txtContra.Text.Trim();
                    u = new UsuarioBOL().Login(u);

                    if (u != null)
                    {
                        if (u.tipo == "Administrador")
                        {
                            FrmMenu frm = new FrmMenu(u.cedula);
                            frm.Show();
                            frm.FormClosed += Logout;
                            this.Hide();
                        }
                        else if (u.tipo == "Pasajero")
                        {
                            FrmMenuPasajero frm = new FrmMenuPasajero(u.cedula);
                            frm.Show();
                            frm.FormClosed += Logout;
                            this.Hide();
                        }
                    }
                    else
                    {
                        msgError("Cedula o contraseña incorrecta");
                        txtCedula.Clear();
                        txtContra.Clear();
                    }
                }
                else
                {
                    msgError("Por favor digite su contraseña");
                }
            }
            else
            {
                msgError("Por favor digite su cedula");
            }
        }

        /// <summary>
        /// Muestra mensajes al usuario
        /// </summary>
        /// <param name="msg"></param> mensaje que genera la aplicacion en caso de error o eventualidad
        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        private void txtUsuario_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void txtContra_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }

        /// <summary>
        /// Restablece el formulario de login nuevamente luego de cerrar cesion 
        /// </summary>
        /// <param name="sender"></param> Frame
        /// <param name="e"></param> Evento
        private void Logout(object sender, FormClosedEventArgs e)
        {
            txtCedula.Text = "Cedula";
            txtContra.Text = "Contraseña";
            lblMensaje.Visible = false;
            this.Show();
            lklbRegistrar.Focus();
        }

        private void lklbRegistrar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FrmRegistrarPasajero frm = new FrmRegistrarPasajero();
            frm.Show();
            frm.FormClosed += Logout;
            this.Hide();          
        }

        private void pcbVerContra_MouseDown(object sender, MouseEventArgs e)
        {
            txtContra.UseSystemPasswordChar = false;
            pcbVerContra.Image = Aplicacion.Properties.Resources.icons8_hide_32px;
        }

        private void pcbVerContra_MouseUp(object sender, MouseEventArgs e)
        {
            txtContra.UseSystemPasswordChar = true;
            pcbVerContra.Image = Aplicacion.Properties.Resources.icons8_eye_32px;
        }

        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
            
        }
    }
}

﻿using Aplicacion.BOL;
using Aplicacion.DAL;
using Aplicacion.Entities;
using Aplicacion.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion
{
    public partial class FrmMenu : Form
    {

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        Usuario u;

        public FrmMenu(string cedula)
        {
            InitializeComponent();
            CenterToScreen();
            u = new UsuarioBOL().CargarUsuario(cedula);
            CargarUsuario();
        }

        /// <summary>
        /// Carga los datos del administrador
        /// </summary>
        private void CargarUsuario()
        {
            mspAdmin.Visible = true;
            lblNombre.Text = u.nombre;
            lblCedula.Text = u.tipo;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void FrmMenu_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void mspAdmin_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }


        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar sesion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Restablece el formulario de menu nuevamente luego de salir de una ventanna 
        /// </summary>
        /// <param name="sender"></param> Frame
        /// <param name="e"></param> Evento
        private void Logout(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void TsmAerolineas_Click(object sender, EventArgs e)
        {
            FrmAerolineas frm = new FrmAerolineas();
            frm.Show();
            frm.FormClosed += Logout;
            this.Hide();
        }

        private void TsmAviones_Click(object sender, EventArgs e)
        {
            FrmAviones frm = new FrmAviones();
            frm.Show();
            frm.FormClosed += Logout;
            this.Hide();
        }

        private void TsmAeropuertos_Click(object sender, EventArgs e)
        {
            FrmAeropuertos frm = new FrmAeropuertos();
            frm.Show();
            frm.FormClosed += Logout;
            this.Hide();
        }

        private void TsmVuelos_Click(object sender, EventArgs e)
        {
            FrmVuelos frm = new FrmVuelos();
            frm.Show();
            frm.FormClosed += Logout;
            this.Hide();
        }

        private void TsmTripulaciones_Click(object sender, EventArgs e)
        {
            FrmTripulaciones frm = new FrmTripulaciones();
            frm.Show();
            frm.FormClosed += Logout;
            this.Hide();
        }

        private void reportesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmReportes frm = new FrmReportes();
            frm.Show();
            frm.FormClosed += Logout;
            this.Hide();
        }
    }
}

﻿using Aplicacion.BOL;
using Aplicacion.DAL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.GUI
{
    public partial class FrmReportes : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        public FrmReportes()
        {
            InitializeComponent();
            CenterToScreen();
            cbxAerolinea.DataSource = new AerolineaBOL().CargarAerolineas();
            cbxAerolineas2.DataSource = new AerolineaBOL().CargarAerolineas();
            MostrarAviones();
        }

        private void FrmAeropuertos_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// Muestra mensajes al usuario
        /// </summary>
        /// <param name="msg"></param> mensaje que genera la aplicacion en caso de error o eventualidad
        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Muestra vuelos segun el filtro de fechas
        /// </summary>
        public void MostrarVFechas()
        {
            dgvVFechas.Rows.Clear();
            string f1 = dtpFecha1.Value.Date.ToString();
            string f2 = dtpFecha2.Value.Date.ToString();
            foreach (Vuelo v in new VueloBOL().CargarReportes(f1,f2))
            {
                dgvVFechas.Rows.Add(v.id, v.aerolinea, v.precio, v.fechaSalida.ToShortDateString(), v.horaSalida,
                    v.aeropuertoSalida.iata, v.fechaLlegada.ToShortDateString(), v.horaLlegada, v.aeropuertoLlegada.iata,
                    v.tiempoVuelo, v.avion, v.piloto1.nombre, v.piloto2.nombre, v.tripulante1.nombre, v.tripulante2.nombre,
                    v.tripulante3.nombre);
            }
        }

        /// <summary>
        /// Muestra los tripulantes de una aerolinea
        /// </summary>
        public void MostarTAerolineas()
        {
            dgvTripulantes.Rows.Clear();
            Aerolinea a = (Aerolinea)cbxAerolineas2.SelectedItem;
            foreach (Tripulante t in new TripulanteBOL().CargarReportes4(a.id))
            {
                dgvTripulantes.Rows.Add(t.id, t.cedula, t.nombre, t.fechaNacimiento.ToShortDateString(),
                    t.aerolinea.nombre, t.rol, t.estado==true?"Activo":"Inactivo");
            }
        }

        /// <summary>
        /// Muestra los vuelos de una aerolinea
        /// </summary>
        public void MostrarVAerolinea()
        {
            dgvVAerolinea.Rows.Clear();
            Aerolinea a = (Aerolinea)cbxAerolinea.SelectedItem;
            foreach (Vuelo v in new VueloBOL().CargarReportes2(a))
            {
                dgvVAerolinea.Rows.Add(v.id, v.aerolinea.nombre, v.precio, v.fechaSalida.ToShortDateString(), v.horaSalida,
                    v.aeropuertoSalida.iata, v.fechaLlegada.ToShortDateString(), v.horaLlegada, v.aeropuertoLlegada.iata, v.tiempoVuelo,
                    v.avion, v.piloto1.nombre, v.piloto2.nombre, v.tripulante1.nombre, v.tripulante2.nombre,
                    v.tripulante3.nombre);

            }
        }

        /// <summary>
        /// Muestra el ranking de vuelos de los aviones
        /// </summary>
        public void MostrarAviones()
        {
            dgvAviones.Rows.Clear();
            foreach (var a in new AvionBOL().CargarReportes3())
            {
                dgvAviones.Rows.Add(a);
            }
        }

        private void btnBFecha_Click(object sender, EventArgs e)
        {
            if ((dtpFecha1.Value.Date < dtpFecha2.Value.Date)&&(dtpFecha1.Value.Date != dtpFecha2.Value.Date))
            {
                MostrarVFechas();
            }
            else
            {
                msgError("La primera fecha \n" +
                    "no puede ser mayor a la segunda\n" +
                    "ni pueden ser iguales");
            }
        }

        private void btnBVuelos_Click(object sender, EventArgs e)
        {
            MostrarVAerolinea();
        }

        private void btnBTrip_Click(object sender, EventArgs e)
        {
            MostarTAerolineas();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (Vuelo item in new VueloDAL().ConsultarDatos())
            {
                Console.WriteLine(item.ToString());
            }
        }
    }
}

﻿using Aplicacion.BOL;
using Aplicacion.DAL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.GUI
{
    public partial class FrmVuelos : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        Aerolinea aerolineaSelec;

        public FrmVuelos()
        {
            InitializeComponent();
            CenterToScreen();
            cbxALlegada.Items.Add("Seleccione");
            cbxASalida.Items.Add("Seleccione");
            cbxAerolinea.DataSource = new AerolineaBOL().CargarAerolineas();
            cbxALlegada.DataSource = new AeropuertoBOL().CargarAeropuertos();
            cbxASalida.DataSource = new AeropuertoBOL().CargarAeropuertos();
            MostrarVuelos();
            dtpHorasVuelo.Value = DateTime.Parse("15/07/2020 00:00");
        }

        /// <summary>
        /// Muestra los vuelos
        /// </summary>
        public void MostrarVuelos()
        {
            dgvVuelos.Rows.Clear();
            foreach (Vuelo v in new VueloBOL().CargarVuelo())
            {
                dgvVuelos.Rows.Add(v.id, v.aerolinea, v.precio, v.fechaSalida.ToShortDateString(),v.horaSalida,
                    v.aeropuertoSalida.iata, v.fechaLlegada.ToShortDateString(), v.horaLlegada, 
                    v.aeropuertoLlegada.iata,v.tiempoVuelo);
            }
        }

        private void FrmVuelos_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Muestra mensajes al usuario
        /// </summary>
        /// <param name="msg"></param> mensaje que genera la aplicacion en caso de error o eventualidad
        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Limpia el formulario
        /// </summary>
        private void limpiar()
        {
            txtPrecio.Clear();
            dtpHoraS.ResetText();
            dtpFechaSalida.ResetText();
            dtpHoraL.ResetText();
            dtpFechaLlegada.ResetText();
            cbxAerolinea.Text = "Seleccione";
            cbxALlegada.Text = "Seleccione";
            cbxASalida.Text = "Seleccione";
            lblMensaje.Visible = false;
        }
        
        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtPrecio.Text != null)
            {
                Vuelo v = new Vuelo();
                v.aerolinea = (Aerolinea)cbxAerolinea.SelectedItem;
                v.precio = Double.Parse(txtPrecio.Text.Trim());
                v.fechaSalida = dtpFechaSalida.Value;
                v.horaSalida = TimeSpan.Parse(dtpHoraS.Value.ToShortTimeString());
                v.aeropuertoSalida = (Aeropuerto)cbxASalida.SelectedItem;
                v.fechaLlegada = dtpFechaLlegada.Value;
                v.horaLlegada = TimeSpan.Parse(dtpHoraL.Value.ToShortTimeString());
                v.aeropuertoLlegada = (Aeropuerto)cbxALlegada.SelectedItem;
                v.avion = new AvionDAL().CargarAvionVuelo(aerolineaSelec.id);
                if (v.avion == null)
                {
                    MessageBox.Show("Aviones insuficientes");
                    limpiar();
                    return;
                }
                v.avion.estado = false;
                new AvionDAL().CambiarEstado(v.avion);
                
                int contPil = 0;
                foreach (Tripulante item in new TripulanteDAL().CargarPilotos(aerolineaSelec.id))
                {
                    if (contPil == 0)
                    {
                        v.piloto1 = item;
                        if (v.piloto1 == null)
                        {
                            MessageBox.Show("Pilotos insuficientes");
                            limpiar();
                            return; 
                        }
                        v.piloto1.estado = false;
                        new TripulanteDAL().CambiarEstado(v.piloto1);
                    }
                    if (contPil == 1)
                    {
                        v.piloto2 = item;
                        if (v.piloto2 == null)
                        {
                            MessageBox.Show("Pilotos insuficientes");
                            limpiar();
                            return;
                        }
                        v.piloto2.estado = false;
                        new TripulanteDAL().CambiarEstado(v.piloto2);
                    }
                    contPil++;
                }

                int contTrip = 0;
                foreach (Tripulante item in new TripulanteDAL().CargarTripulantes(aerolineaSelec.id))
                {
                    if (contTrip == 0)
                    {
                        v.tripulante1 = item;
                        if (v.tripulante1 == null)
                        {
                            MessageBox.Show("Tripulantes insuficientes");
                            limpiar();
                            return;
                        }
                        v.tripulante1.estado = false;
                        new TripulanteDAL().CambiarEstado(v.tripulante1);
                    }
                    if (contTrip == 1)
                    {
                        v.tripulante2 = item;
                        if (v.tripulante2 == null)
                        {
                            MessageBox.Show("Tripulantes insuficientes");
                            limpiar();
                            return;
                        }
                        v.tripulante2.estado = false;
                        new TripulanteDAL().CambiarEstado(v.tripulante2);
                    }
                    else if (contTrip == 2)
                    {
                        v.tripulante3 = item;
                        if (v.tripulante3 == null)
                        {
                            MessageBox.Show("Tripulantes insuficientes");
                            limpiar();
                            return;
                        }
                        v.tripulante3.estado = false;
                        new TripulanteDAL().CambiarEstado(v.tripulante3);
                    }
                    contTrip++;
                }

                TimeSpan duracionVuelo = TimeSpan.Parse(dtpHorasVuelo.Value.ToShortTimeString());
                v.tiempoVuelo = duracionVuelo;
                Console.WriteLine(v.tiempoVuelo.ToString());
                new VueloBOL().Insertar(v);
                MostrarVuelos();
                MessageBox.Show("Vuelo registrado", "Información");
                limpiar();
                
            }
            else
            {
                msgError("Debe llenar todos los campos");
            }
            
        }

        private void cbxAerolinea_SelectedIndexChanged(object sender, EventArgs e)
        {
            limpiar();
            if (cbxAerolinea.SelectedValue.ToString() != null)
            {
                aerolineaSelec = (Aerolinea)cbxAerolinea.SelectedItem;

            }
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

    }
}

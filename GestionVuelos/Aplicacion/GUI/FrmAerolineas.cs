﻿using Aplicacion.BOL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.GUI
{
    public partial class FrmAerolineas : Form
    {

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        public FrmAerolineas()
        {
            InitializeComponent();
            CenterToScreen();
            MostrarAerolineas();
            dtpFechaFundacion.ShowUpDown = true;

        }

        /// <summary>
        /// Carga desde la base de datos las aerolineas
        /// </summary>
        public void MostrarAerolineas()
        {
            dgvAerolineas.Rows.Clear();
            foreach (Aerolinea a in new AerolineaBOL().CargarAerolineas())
            {
                dgvAerolineas.Rows.Add(a, a.nombre, a.annoFundacion, a.tipo);

            }
        }

        private void FrmAerolineas_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != "")
            {
                if (dtpFechaFundacion.Value.Year < 2021)
                {
                    if (cbxTipo.Text != "Seleccione")
                    {
                        Aerolinea a = new Aerolinea();
                        a.nombre = txtNombre.Text.Trim();
                        a.annoFundacion = dtpFechaFundacion.Value.Year.ToString();
                        a.tipo = cbxTipo.Text;
                        new AerolineaBOL().Insertar(a);
                        MostrarAerolineas();
                        MessageBox.Show("Aerolinea registrada", "Informacion");
                        limpiar();
                    }
                    else
                    {
                        msgError("Seleccione un tipo");
                    }
                }
                else
                {
                    msgError("No puede ser mayor al año actual");
                }
            }
            else
            {
                msgError("Digite el nombre");
            }
        }

        /// <summary>
        /// Muestra mensajes al usuario
        /// </summary>
        /// <param name="msg"></param> mensaje que genera la aplicacion en caso de error o eventualidad
        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Limpia los componentes de la pantalla
        /// </summary>
        private void limpiar()
        {
            txtNombre.Clear();
            dtpFechaFundacion.ResetText();
            cbxTipo.Text = "Seleccione";
            lblMensaje.Visible = false;
        }

        private void txtNombre_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void cbxTipo_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }
    }
}

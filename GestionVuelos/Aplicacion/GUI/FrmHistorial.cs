﻿using Aplicacion.BOL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.GUI
{
    public partial class FrmHistorial : Form
    {

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        string cedulaPasajero;

        public FrmHistorial(string ced)
        {
            InitializeComponent();
            CenterToScreen();
            cedulaPasajero = ced;
            cargar();
        }

        /// <summary>
        /// Carga el historial de vuelos del pasajero que esta usando la aplicacion
        /// </summary>
        private void cargar()
        {
            dgvHistorial.Rows.Clear();
            foreach (Historial h in new HistorialBOL().CargarHistorial(cedulaPasajero))
            {
                dgvHistorial.Rows.Add(h, h.cedula, h.nombrePaisSalida, h.nombrePaisLlegada,
                    h.nombrePaisEscala1, h.nombrePaisEscala2, h.fechaCompraVuelo.ToShortDateString(), h.horaCompraVuelo,
                    h.tiempoVuelo, h.costoTotal);
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void FrmHistorial_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

    }
}

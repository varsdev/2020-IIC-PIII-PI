﻿using Aplicacion.BOL;
using Aplicacion.DAL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.GUI
{
    public partial class FrmMenuPasajero : Form
    {

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        Usuario u;

        Vuelo vuelo;

        Vuelo escala1;
        Vuelo escala2;
        Vuelo escala3;
        public FrmMenuPasajero(string cedula)
        {
            InitializeComponent();
            u = new UsuarioBOL().CargarUsuario(cedula);
            CenterToScreen();
            CargarUsuario();
            Cargar();
        }

        /// <summary>
        /// Carga los datos del pasajero
        /// </summary>
        private void CargarUsuario()
        {
            lblNombre.Text = u.nombre;
            lblCedula.Text = u.cedula;
            lblEdad.Text = u.edad.ToString();
        }

        /// <summary>
        /// Carga los combobox con los aeropuertos
        /// </summary>
        private void Cargar()
        {
            cbxALlegada.DataSource = new AeropuertoBOL().CargarAeropuertos();
            cbxASalida.DataSource = new AeropuertoBOL().CargarAeropuertos();
        }

        /// <summary>
        /// Limpia el formulario
        /// </summary>
        private void limpiar()
        {
            btnComprar.Enabled = false;
            lblMensaje.Visible = false;
            dgvVuelos.Rows.Clear();
            dtpFechaLlegada.ResetText();
            dtpFechaSalida.ResetText();           
            rdbPrecio.Checked = false;
            rdbTiempo.Checked = false;
            radioButton1.Checked = true;
            Cargar();
        }

        /// <summary>
        /// Muestra los vuelos que almacena la lista
        /// </summary>
        /// <param name="lista"></param> lista de vuelos previamente consultados a la BD
        private void mostrar(List<Vuelo> lista)
        {
            dgvVuelos.Rows.Clear();
            foreach (Vuelo v in lista)
            {
                dgvVuelos.Rows.Add(v, v.precio, v.tiempoVuelo, v.fechaSalida.ToShortDateString(),
                    v.fechaLlegada.ToShortDateString(),v.aerolinea, v.horaSalida, v.horaLlegada,
                    v.aeropuertoSalida.iata, v.aeropuertoLlegada.iata);
            }
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar sesion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void FrmMenuPasajero_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void Logout(object sender, FormClosedEventArgs e)
        {
            dgvVuelos.Rows.Clear();
            this.Show();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            btnComprar.Enabled = false;
            Aeropuerto a1 = new Aeropuerto();
            Aeropuerto a2 = new Aeropuerto();
            a1 = (Aeropuerto)cbxASalida.SelectedItem;
            a2 = (Aeropuerto)cbxALlegada.SelectedItem;
            String f1 = dtpFechaSalida.Value.ToShortDateString();
            String f2 = dtpFechaLlegada.Value.ToShortDateString();
            if (rdbPrecio.Checked)
            {
                List<Vuelo> vuelos = new VueloBOL().CargarVuelosPrecio(f1, f2, a1.id, a2.id);
                if (vuelos.Count == 0)
                {
                    List<Vuelo> esc = CargarEscalaNormal(f1, f2, a1.id, a2.id);
                    if (esc.Count <= 1)
                    {
                        List<Vuelo> esc2 = CargarEscalaDoble(f1, f2, a1.id, a2.id);
                        if (esc2.Count <= 2)
                        {
                            msgError("Vuelo no disponible");
                        }
                    }                  
                }
                if (vuelos.Count > 0)
                {
                    mostrar(vuelos);
                    msgError("Vuelo encontrado");
                }
            }
            else if (rdbTiempo.Checked)
            {
                List<Vuelo> vuelos = new VueloBOL().CargarVuelosTiempo(f1, f2, a1.id, a2.id);
                if (vuelos.Count == 0)
                {
                    List<Vuelo> esc = CargarEscalaNormal(f1, f2, a1.id, a2.id);
                    if (esc.Count <= 1)
                    {
                        List<Vuelo> esc2 = CargarEscalaDoble(f1, f2, a1.id, a2.id);
                        if (esc2.Count <= 2)
                        {
                            msgError("Vuelo no disponible");
                        }
                    }
                }
                if (vuelos.Count > 0)
                {
                    mostrar(vuelos);
                    msgError("Vuelo encontrado");
                }
            }
            else
            {
                List<Vuelo> vuelos = new VueloBOL().CargarVuelosPasajeros(f1, f2, a1.id, a2.id);
                if (vuelos.Count == 0)
                {
                    List<Vuelo> esc = CargarEscalaNormal(f1, f2, a1.id, a2.id);
                    if (esc.Count <= 1)
                    {
                        List<Vuelo> esc2 = CargarEscalaDoble(f1, f2, a1.id, a2.id);
                        if (esc2.Count <= 2)
                        {
                            msgError("Vuelo no disponible");
                        }
                    }
                }
                if (vuelos.Count > 0)
                {
                    mostrar(vuelos);
                    msgError("Vuelo encontrado");
                }
            }
        }

        private void btnBusquedaInteligente_Click(object sender, EventArgs e)
        {
            FrmBusquedaInteligente frm = new FrmBusquedaInteligente();
            frm.Show();
            frm.FormClosed += Logout;
            this.Hide();
        }

        private void btnHistorial_Click(object sender, EventArgs e)
        {
            FrmHistorial frm = new FrmHistorial(u.cedula);
            frm.Show();
            frm.FormClosed += Logout;
            this.Hide();
        }

        /// <summary>
        /// Consulta a la base de datos los vuelos para poder crear una escala y cargarla de ser posible
        /// </summary>
        /// <param name="f1"></param> filtro de fecha salida
        /// <param name="f2"></param> filtro de fecha regreso
        /// <param name="id1"></param> filtro de aeropuerto salida
        /// <param name="id2"></param> filtro de aeropuerto regreso
        /// <returns></returns> lista con vuelos de escala
        private List<Vuelo> CargarEscalaNormal(String f1, String f2, int id1, int id2)
        {

            List<Vuelo> Escalas = new VueloBOL().CargarEscalasNormales(f1, f2, id1, id2);
            int cont = 0;

            dgvVuelos.Rows.Clear();
            foreach (Vuelo v in Escalas)
            {

                if (cont == 0)
                {
                    escala1 = v;
                }
                if (cont == 1)
                {
                    escala2 = v;

                    if (escala1 != null && escala2 != null)
                    {
                        string id = "0";
                        string aerolinea = escala1.aerolinea.nombre;
                        string precio = (escala1.precio + escala2.precio).ToString();
                        string fechaSalida = escala1.fechaSalida.ToShortDateString();
                        string horaSalida = escala1.horaSalida.ToString();
                        string aeropuertoSalida = escala1.aeropuertoSalida.iata + " - " + escala1.aeropuertoLlegada.iata;
                        string fechaLlegada = escala2.fechaLlegada.ToShortDateString();
                        string horaLlegada = escala2.horaLlegada.ToString();
                        string aeropuertoLlegada = escala2.aeropuertoSalida.iata + " - " + escala2.aeropuertoLlegada.iata;
                        string tiempoVuelo = (escala1.tiempoVuelo + escala2.tiempoVuelo).ToString();
                        dgvVuelos.Rows.Add(id, precio, tiempoVuelo, fechaSalida,
                        fechaLlegada, aerolinea, horaSalida, horaLlegada,
                        aeropuertoSalida, aeropuertoLlegada);
                        msgError("Escala encontrada");
                    }
                    else
                    {
                        msgError("Vuelo no disponible");
                        escala1 = null;
                        escala2 = null;
                        escala3 = null;
                    }

                }

                cont++;

            }
            return Escalas;
        }

        /// <summary>
        /// Consulta a la base de datos los vuelos para poder crear una escala doble y cargarla de ser posible
        /// </summary>
        /// <param name="f1"></param> filtro de fecha salida
        /// <param name="f2"></param> filtro de fecha regreso
        /// <param name="id1"></param> filtro de aeropuerto salida
        /// <param name="id2"></param> filtro de aeropuerto regreso
        /// <returns></returns> lista con vuelos de escala
        private List<Vuelo> CargarEscalaDoble(String f1, String f2, int id1, int id2)
        {

            List<Vuelo> Escalas = new VueloBOL().CargarEscalasDobles(f1, f2, id1, id2);
            int cont = 0;

            dgvVuelos.Rows.Clear();
            foreach (Vuelo v in Escalas)
            {

                if (cont == 0)
                {
                    escala1 = v;
                }
                if (cont == 1)
                {
                    escala2 = v;
                }
                if (cont == 2)
                {
                    escala3 = v;

                    if (escala1 != null && escala2 != null && escala3 != null)
                    {
                        string id = "0";
                        string aerolinea = escala1.aerolinea.nombre;
                        string precio = (escala1.precio + escala2.precio + escala3.precio).ToString();
                        string fechaSalida = escala1.fechaSalida.ToShortDateString();
                        string horaSalida = escala1.horaSalida.ToString();
                        string aeropuertoSalida = escala1.aeropuertoSalida.iata + " - " + escala1.aeropuertoLlegada.iata + " - " + escala2.aeropuertoSalida.iata;
                        string fechaLlegada = escala2.fechaLlegada.ToShortDateString();
                        string horaLlegada = escala2.horaLlegada.ToString();
                        string aeropuertoLlegada = escala2.aeropuertoLlegada.iata + " - " + escala3.aeropuertoSalida.iata + " - " + escala3.aeropuertoLlegada.iata;
                        string tiempoVuelo = (escala1.tiempoVuelo + escala2.tiempoVuelo + escala3.tiempoVuelo).ToString();
                        dgvVuelos.Rows.Add(id, precio, tiempoVuelo, fechaSalida,
                        fechaLlegada, aerolinea, horaSalida, horaLlegada,
                        aeropuertoSalida, aeropuertoLlegada);
                        msgError("Escala encontrada");
                    }
                    else
                    {
                        msgError("Vuelo no disponible");
                        escala1 = null;
                        escala2 = null;
                        escala3 = null;
                    }
                    
                }

                cont++;

            }
            return Escalas;
        }

        /// <summary>
        /// Muestra mensajes al usuario
        /// </summary>
        /// <param name="msg"></param> mensaje que genera la aplicacion en caso de error o eventualidad
        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        private void dgvVuelos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                lblMensaje.Visible = false;
                if (escala1 != null && escala2 != null)
                {
                    btnComprar.Enabled = true;
                }
                vuelo = (Vuelo)dgvVuelos.Rows[e.RowIndex].Cells[0].Value;

                if (vuelo != null)
                {
                    btnComprar.Enabled = true;
                }

            }
            catch (Exception)
            {

            }
        }

        private void btnComprar_Click(object sender, EventArgs e)
        {
            if (vuelo != null)
            {
                Historial h = new Historial();
                h.cedula = u.cedula;
                h.nombrePaisSalida = vuelo.aeropuertoSalida.pais;
                h.nombrePaisLlegada = vuelo.aeropuertoLlegada.pais;
                h.nombrePaisEscala1 = "Sin escala";
                h.nombrePaisEscala2 = "Sin escala";
                DateTime fecha = DateTime.Now;
                h.fechaCompraVuelo = fecha;
                h.horaCompraVuelo = TimeSpan.Parse(fecha.ToLongTimeString());
                h.tiempoVuelo = vuelo.tiempoVuelo;
                h.costoTotal = vuelo.precio;
                if (MessageBox.Show("Seguro que desea realizar la compra ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    new HistorialBOL().Insertar(h);
                    MessageBox.Show("Vuelo comprado");

                    //Vuelo
                    vuelo.avion.estado = true;
                    new AvionDAL().CambiarEstado(vuelo.avion);

                    vuelo.piloto1.estado = true;
                    new TripulanteDAL().CambiarEstado(vuelo.piloto1);

                    vuelo.piloto2.estado = true;
                    new TripulanteDAL().CambiarEstado(vuelo.piloto2);

                    vuelo.tripulante1.estado = true;
                    new TripulanteDAL().CambiarEstado(vuelo.tripulante1);

                    vuelo.tripulante2.estado = true;
                    new TripulanteDAL().CambiarEstado(vuelo.tripulante2);

                    vuelo.tripulante3.estado = true;
                    new TripulanteDAL().CambiarEstado(vuelo.tripulante3);
                }
                vuelo = null;
                limpiar();
                return;
            }
            if(escala1 != null && escala2 != null && escala3 == null)
            {
                Historial h = new Historial();
                h.cedula = u.cedula;
                h.nombrePaisSalida = escala1.aeropuertoSalida.pais;
                h.nombrePaisLlegada = escala2.aeropuertoLlegada.pais;
                h.nombrePaisEscala1 = escala1.aeropuertoLlegada.pais;
                h.nombrePaisEscala2 = "Sin escala";
                DateTime fecha = DateTime.Now;
                h.fechaCompraVuelo = fecha;
                h.horaCompraVuelo = TimeSpan.Parse(fecha.ToShortTimeString());
                h.tiempoVuelo = escala1.tiempoVuelo + escala2.tiempoVuelo;
                h.costoTotal = escala1.precio + escala2.precio;
                if (MessageBox.Show("Seguro que desea realizar la compra ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    new HistorialBOL().Insertar(h);
                    MessageBox.Show("Vuelo comprado");

                    //Escala #1
                    escala1.avion.estado = true;
                    new AvionDAL().CambiarEstado(escala1.avion);

                    escala1.piloto1.estado = true;
                    new TripulanteDAL().CambiarEstado(escala1.piloto1);

                    escala1.piloto2.estado = true;
                    new TripulanteDAL().CambiarEstado(escala1.piloto2);

                    escala1.tripulante1.estado = true;
                    new TripulanteDAL().CambiarEstado(escala1.tripulante1);

                    escala1.tripulante2.estado = true;
                    new TripulanteDAL().CambiarEstado(escala1.tripulante2);

                    escala1.tripulante3.estado = true;
                    new TripulanteDAL().CambiarEstado(escala1.tripulante3);

                    //Escala #2
                    escala2.avion.estado = true;
                    new AvionDAL().CambiarEstado(escala2.avion);

                    escala2.piloto1.estado = true;
                    new TripulanteDAL().CambiarEstado(escala2.piloto1);

                    escala2.piloto2.estado = true;
                    new TripulanteDAL().CambiarEstado(escala2.piloto2);

                    escala2.tripulante1.estado = true;
                    new TripulanteDAL().CambiarEstado(escala2.tripulante1);

                    escala2.tripulante2.estado = true;
                    new TripulanteDAL().CambiarEstado(escala2.tripulante2);

                    escala2.tripulante3.estado = true;
                    new TripulanteDAL().CambiarEstado(escala2.tripulante3);
                }              
                escala1 = null;
                escala2 = null;
                limpiar();
                return;
            }
            if (escala1 != null && escala2 != null && escala3 != null)
            {
                Historial h = new Historial();
                h.cedula = u.cedula;
                h.nombrePaisSalida = escala1.aeropuertoSalida.pais;
                h.nombrePaisLlegada = escala3.aeropuertoLlegada.pais;
                h.nombrePaisEscala1 = escala2.aeropuertoSalida.pais;
                h.nombrePaisEscala2 = escala2.aeropuertoLlegada.pais;
                DateTime fecha = DateTime.Now;
                h.fechaCompraVuelo = fecha;
                h.horaCompraVuelo = TimeSpan.Parse(fecha.ToShortTimeString());
                h.tiempoVuelo = escala1.tiempoVuelo + escala2.tiempoVuelo + escala3.tiempoVuelo;
                h.costoTotal = escala1.precio + escala2.precio + escala3.precio;
                if (MessageBox.Show("Seguro que desea realizar la compra ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    new HistorialBOL().Insertar(h);
                    MessageBox.Show("Vuelo comprado");

                    //Escala #1
                    escala1.avion.estado = true;
                    new AvionDAL().CambiarEstado(escala1.avion);

                    escala1.piloto1.estado = true;
                    new TripulanteDAL().CambiarEstado(escala1.piloto1);

                    escala1.piloto2.estado = true;
                    new TripulanteDAL().CambiarEstado(escala1.piloto2);

                    escala1.tripulante1.estado = true;
                    new TripulanteDAL().CambiarEstado(escala1.tripulante1);

                    escala1.tripulante2.estado = true;
                    new TripulanteDAL().CambiarEstado(escala1.tripulante2);

                    escala1.tripulante3.estado = true;
                    new TripulanteDAL().CambiarEstado(escala1.tripulante3);

                    //Escala #2
                    escala2.avion.estado = true;
                    new AvionDAL().CambiarEstado(escala2.avion);

                    escala2.piloto1.estado = true;
                    new TripulanteDAL().CambiarEstado(escala2.piloto1);

                    escala2.piloto2.estado = true;
                    new TripulanteDAL().CambiarEstado(escala2.piloto2);

                    escala2.tripulante1.estado = true;
                    new TripulanteDAL().CambiarEstado(escala2.tripulante1);

                    escala2.tripulante2.estado = true;
                    new TripulanteDAL().CambiarEstado(escala2.tripulante2);

                    escala2.tripulante3.estado = true;
                    new TripulanteDAL().CambiarEstado(escala2.tripulante3);

                    //Escala #3
                    escala3.avion.estado = true;
                    new AvionDAL().CambiarEstado(escala3.avion);

                    escala3.piloto1.estado = true;
                    new TripulanteDAL().CambiarEstado(escala3.piloto1);

                    escala3.piloto2.estado = true;
                    new TripulanteDAL().CambiarEstado(escala3.piloto2);

                    escala3.tripulante1.estado = true;
                    new TripulanteDAL().CambiarEstado(escala3.tripulante1);

                    escala3.tripulante2.estado = true;
                    new TripulanteDAL().CambiarEstado(escala3.tripulante2);

                    escala3.tripulante3.estado = true;
                    new TripulanteDAL().CambiarEstado(escala3.tripulante3);
                }
                escala1 = null;
                escala2 = null;
                escala3 = null;
                limpiar();
                return;
            }

        }

        private void cbxALlegada_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void cbxASalida_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void dtpFechaSalida_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void dtpFechaLlegada_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void radioButton1_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void rdbTiempo_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void rdbPrecio_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

    }
}

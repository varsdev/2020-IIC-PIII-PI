﻿using Aplicacion.BOL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.GUI
{
    public partial class FrmTripulaciones : Form
    {

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        public FrmTripulaciones()
        {
            InitializeComponent();
            CenterToScreen();
            cbxAerolinea.DataSource = new AerolineaBOL().CargarAerolineas();
            MostrarTripulantes();
        }

        /// <summary>
        /// Muestra los tripulantes
        /// </summary>
        public void MostrarTripulantes()
        {
            dgvTripulantes.Rows.Clear();
            foreach (Tripulante t in new TripulanteBOL().CargarTripulantes())
            {
                dgvTripulantes.Rows.Add(t.id,t.cedula,t.nombre,t.fechaNacimiento.ToShortDateString(),t.aerolinea,t.rol,
                    t.estado==true?"Activo":"Inactivo");

            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void FrmTripulaciones_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Muestra mensajes al usuario
        /// </summary>
        /// <param name="msg"></param> mensaje que genera la aplicacion en caso de error o eventualidad
        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Limpia el formulario
        /// </summary>
        private void limpiar()
        {
            txtCedula.Clear();
            txtNombre.Clear();
            dtpFechaNacim.ResetText();
            cbxAerolinea.Text = "Seleccione";
            cbxRoles.Text = "Seleccione";
            lblMensaje.Visible = false;
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (cbxRoles.Text != "Seleccione")
            {
                Tripulante t = new Tripulante();
                t.cedula = txtCedula.Text.Trim();
                t.nombre = txtNombre.Text.Trim();
                t.fechaNacimiento = dtpFechaNacim.Value.Date;
                t.aerolinea = (Aerolinea)cbxAerolinea.SelectedItem;
                t.rol = cbxRoles.SelectedItem.ToString();
                t.estado = true;
                new TripulanteBOL().Insertar(t);
                MostrarTripulantes();
                MessageBox.Show("Tripulante registrado", "Información");
                limpiar();

            }
            else
            {
                msgError("Seleccione una aerolinea y un rol");
            }
        }

        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

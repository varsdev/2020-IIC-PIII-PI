﻿using Aplicacion.BOL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.GUI
{
    public partial class FrmBusquedaInteligente : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        public FrmBusquedaInteligente()
        {
            InitializeComponent();
            CenterToScreen();
            Cargar();
        }

        /// <summary>
        /// Carga los vuelos que recibe de una lista
        /// </summary>
        /// <param name="lista"></param> lista de vuelos previamente consultada a la BD
        private void mostrar(List<Vuelo> lista)
        {
            dgvVuelos.Rows.Clear();
            foreach (Vuelo v in lista)
            {
                dgvVuelos.Rows.Add(v.id, v.fechaSalida.ToShortDateString(), v.fechaLlegada.ToShortDateString(),
                    v.precio, v.tiempoVuelo, v.aerolinea, v.horaSalida, v.horaLlegada,
                    v.aeropuertoSalida.iata, v.aeropuertoLlegada.iata);
            }
        }

        /// <summary>
        /// Carga los combobox con los aeropuertos
        /// </summary>
        private void Cargar()
        {
            cbxALlegada.DataSource = new AeropuertoBOL().CargarAeropuertos();
            cbxASalida.DataSource = new AeropuertoBOL().CargarAeropuertos();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void FrmBusquedaInteligente_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            double dias;
            Aeropuerto a1 = new Aeropuerto();
            Aeropuerto a2 = new Aeropuerto();
            a1 = (Aeropuerto)cbxASalida.SelectedItem;
            a2 = (Aeropuerto)cbxALlegada.SelectedItem;
            if (txtRangoDias.Text == "")
            {
                dias = 0;
            }
            else
            {
                dias = double.Parse(txtRangoDias.Text.Trim());
            }

            String f1 = dtpFechaSalida.Value.AddDays(-dias).ToShortDateString();
            String f2 = dtpFechaSalida.Value.AddDays(dias).ToShortDateString();

            String f3 = dtpFechaLlegada.Value.AddDays(-dias).ToShortDateString();
            String f4 = dtpFechaLlegada.Value.AddDays(dias).ToShortDateString();

            List<Vuelo> vuelos = new VueloBOL().CargarVuelosInteligentes(f1, f2, f3, f4, a1.id, a2.id);

            if (vuelos.Count >= 0)
            {
                msgError("Vuelo encontrado");
            }
            if(vuelos.Count == 0)
            {
                msgError("Vuelo no disponible");
            }
            mostrar(vuelos);

        }

        private void txtRangoDias_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void cbxASalida_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void cbxALlegada_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void dtpFechaSalida_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void dtpFechaLlegada_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void txtRangoDias_Enter(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
        }

        /// <summary>
        /// Muestra mensajes al usuario
        /// </summary>
        /// <param name="msg"></param> mensaje que genera la aplicacion en caso de error o eventualidad
        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }
    }
}

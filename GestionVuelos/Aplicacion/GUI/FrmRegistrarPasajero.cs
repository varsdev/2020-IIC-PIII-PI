﻿using Aplicacion.BOL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.GUI
{
    public partial class FrmRegistrarPasajero : Form
    {
        public FrmRegistrarPasajero()
        {
            InitializeComponent();
            CenterToScreen();
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtCedula.Text != "")
            {
                if (txtNombre.Text != "")
                {
                    if (txtEdad.Text != "")
                    {
                        if (txtContra.Text != "")
                        {
                            if (txtReContra.Text != "")
                            {
                                Usuario u = new Usuario();
                                u.cedula = txtCedula.Text.Trim();
                                u.nombre = txtNombre.Text.Trim();
                                u.edad = Int32.Parse(txtEdad.Text.Trim());
                                if (txtContra.Text == txtReContra.Text)
                                {
                                    u.contrasenna = txtReContra.Text;
                                    u.tipo = "Pasajero";
                                    new UsuarioBOL().Insertar(u);
                                    MessageBox.Show("Pasajero Registrado", "Informacion");
  
                                    limpiar();

                                }
                                else
                                {
                                    msgError("La contraseña no es igual");

                                }
                            }
                            else
                            {
                                msgError("Por favor confirme su contraseña");
                            }
                        }
                        else
                        {
                            msgError("Por favor digite su contraseña");
                        }
                    }
                    else
                    {
                        msgError("Por favor digite su edad");
                    }
                }
                else
                {
                    msgError("Por favor digite su nombre");
                }
            }
            else
            {
                msgError("Por favor digite su cedula");
            }
        }

        private void FrmRegistrarPasajero_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        /// <summary>
        /// Muestra mensajes al usuario
        /// </summary>
        /// <param name="msg"></param> mensaje que genera la aplicacion en caso de error o eventualidad
        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        private void txtCedula_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void txtNombre_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void txtEdad_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void txtContra_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void txtReContra_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void pcbVerContra_MouseDown(object sender, MouseEventArgs e)
        {
            txtContra.UseSystemPasswordChar = false;
            pcbVerContra.Image = Aplicacion.Properties.Resources.icons8_hide_32px;
        }

        private void pcbVerContra_MouseUp(object sender, MouseEventArgs e)
        {
            txtContra.UseSystemPasswordChar = true;
            pcbVerContra.Image = Aplicacion.Properties.Resources.icons8_eye_32px;
        }

        private void pcbVerReContra_MouseDown(object sender, MouseEventArgs e)
        {
            txtReContra.UseSystemPasswordChar = false;
            pcbVerReContra.Image = Aplicacion.Properties.Resources.icons8_hide_32px;
        }

        private void pcbVerReContra_MouseUp(object sender, MouseEventArgs e)
        {
            txtReContra.UseSystemPasswordChar = true;
            pcbVerReContra.Image = Aplicacion.Properties.Resources.icons8_eye_32px;
        }

        /// <summary>
        /// Limpiar el formulario
        /// </summary>
        private void limpiar()
        {
            txtCedula.Clear();
            txtNombre.Clear();
            txtEdad.Clear();
            txtContra.Clear();
            txtReContra.Clear();
            lblMensaje.Visible = false;
        }

        private void txtEdad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

﻿namespace Aplicacion.GUI
{
    partial class FrmReportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnVolver = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tp1 = new System.Windows.Forms.TabPage();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.btnBFecha = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFecha2 = new System.Windows.Forms.DateTimePicker();
            this.dtpFecha1 = new System.Windows.Forms.DateTimePicker();
            this.dgvVFechas = new System.Windows.Forms.DataGridView();
            this.cId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAerolinea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cHSalida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraSal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cASalida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cHLlegada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraLLe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cALlegada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTiempoVuelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAvion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPiloto1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPiloto2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTrip1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTrip2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTrip3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp2 = new System.Windows.Forms.TabPage();
            this.cbxAerolinea = new System.Windows.Forms.ComboBox();
            this.btnBVuelos = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvVAerolinea = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraSalida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraLlegada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp3 = new System.Windows.Forms.TabPage();
            this.dgvAviones = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp4 = new System.Windows.Forms.TabPage();
            this.btnBTrip = new System.Windows.Forms.Button();
            this.cbxAerolineas2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvTripulantes = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFechaN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cRol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnMinimizar = new System.Windows.Forms.PictureBox();
            this.btnCerrar = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVFechas)).BeginInit();
            this.tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVAerolinea)).BeginInit();
            this.tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAviones)).BeginInit();
            this.tp4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTripulantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.btnVolver);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(217, 445);
            this.panel1.TabIndex = 14;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Aplicacion.Properties.Resources.icons8_filter_96px;
            this.pictureBox1.Location = new System.Drawing.Point(61, 177);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(95, 96);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // btnVolver
            // 
            this.btnVolver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnVolver.FlatAppearance.BorderSize = 0;
            this.btnVolver.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.btnVolver.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVolver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVolver.ForeColor = System.Drawing.Color.White;
            this.btnVolver.Image = global::Aplicacion.Properties.Resources.icons8_back_arrow_32px;
            this.btnVolver.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVolver.Location = new System.Drawing.Point(0, 404);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(217, 41);
            this.btnVolver.TabIndex = 17;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = false;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.LightGray;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 39);
            this.label1.TabIndex = 14;
            this.label1.Text = "Reportes";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tp1);
            this.tabControl1.Controls.Add(this.tp2);
            this.tabControl1.Controls.Add(this.tp3);
            this.tabControl1.Controls.Add(this.tp4);
            this.tabControl1.Location = new System.Drawing.Point(222, 32);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(569, 402);
            this.tabControl1.TabIndex = 17;
            // 
            // tp1
            // 
            this.tp1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.tp1.Controls.Add(this.lblMensaje);
            this.tp1.Controls.Add(this.btnBFecha);
            this.tp1.Controls.Add(this.label7);
            this.tp1.Controls.Add(this.label3);
            this.tp1.Controls.Add(this.dtpFecha2);
            this.tp1.Controls.Add(this.dtpFecha1);
            this.tp1.Controls.Add(this.dgvVFechas);
            this.tp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp1.Location = new System.Drawing.Point(4, 22);
            this.tp1.Margin = new System.Windows.Forms.Padding(2);
            this.tp1.Name = "tp1";
            this.tp1.Padding = new System.Windows.Forms.Padding(2);
            this.tp1.Size = new System.Drawing.Size(561, 376);
            this.tp1.TabIndex = 0;
            this.tp1.Text = "Vuelo por Rango de Fechas";
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.ForeColor = System.Drawing.Color.LightGray;
            this.lblMensaje.Image = global::Aplicacion.Properties.Resources.icons8_box_important_32px;
            this.lblMensaje.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMensaje.Location = new System.Drawing.Point(310, 17);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(102, 18);
            this.lblMensaje.TabIndex = 81;
            this.lblMensaje.Text = "Mensaje Error";
            this.lblMensaje.Visible = false;
            // 
            // btnBFecha
            // 
            this.btnBFecha.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBFecha.FlatAppearance.BorderSize = 0;
            this.btnBFecha.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.btnBFecha.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBFecha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBFecha.ForeColor = System.Drawing.Color.White;
            this.btnBFecha.Location = new System.Drawing.Point(402, 74);
            this.btnBFecha.Name = "btnBFecha";
            this.btnBFecha.Size = new System.Drawing.Size(148, 46);
            this.btnBFecha.TabIndex = 80;
            this.btnBFecha.Text = "Buscar";
            this.btnBFecha.UseVisualStyleBackColor = false;
            this.btnBFecha.Click += new System.EventHandler(this.btnBFecha_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(23, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 20);
            this.label7.TabIndex = 79;
            this.label7.Text = "Fecha 1:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(23, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 20);
            this.label3.TabIndex = 78;
            this.label3.Text = "Fecha 2:";
            // 
            // dtpFecha2
            // 
            this.dtpFecha2.CustomFormat = "dd/MM/yyyy";
            this.dtpFecha2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecha2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFecha2.Location = new System.Drawing.Point(117, 58);
            this.dtpFecha2.Margin = new System.Windows.Forms.Padding(2);
            this.dtpFecha2.Name = "dtpFecha2";
            this.dtpFecha2.Size = new System.Drawing.Size(111, 26);
            this.dtpFecha2.TabIndex = 69;
            // 
            // dtpFecha1
            // 
            this.dtpFecha1.CustomFormat = "dd/MM/yyyy";
            this.dtpFecha1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecha1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFecha1.Location = new System.Drawing.Point(117, 17);
            this.dtpFecha1.Margin = new System.Windows.Forms.Padding(2);
            this.dtpFecha1.Name = "dtpFecha1";
            this.dtpFecha1.Size = new System.Drawing.Size(111, 26);
            this.dtpFecha1.TabIndex = 68;
            // 
            // dgvVFechas
            // 
            this.dgvVFechas.AllowUserToAddRows = false;
            this.dgvVFechas.AllowUserToDeleteRows = false;
            this.dgvVFechas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(91)))), ((int)(((byte)(149)))));
            this.dgvVFechas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvVFechas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.DarkSlateGray;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVFechas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvVFechas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVFechas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cId,
            this.cAerolinea,
            this.cPrecio,
            this.cHSalida,
            this.HoraSal,
            this.cASalida,
            this.cHLlegada,
            this.HoraLLe,
            this.cALlegada,
            this.cTiempoVuelo,
            this.cAvion,
            this.cPiloto1,
            this.cPiloto2,
            this.cTrip1,
            this.cTrip2,
            this.cTrip3});
            this.dgvVFechas.EnableHeadersVisualStyles = false;
            this.dgvVFechas.GridColor = System.Drawing.Color.DarkCyan;
            this.dgvVFechas.Location = new System.Drawing.Point(0, 139);
            this.dgvVFechas.Name = "dgvVFechas";
            this.dgvVFechas.ReadOnly = true;
            this.dgvVFechas.RowHeadersWidth = 51;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(91)))), ((int)(((byte)(149)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.DarkCyan;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
            this.dgvVFechas.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvVFechas.Size = new System.Drawing.Size(563, 241);
            this.dgvVFechas.TabIndex = 67;
            // 
            // cId
            // 
            this.cId.HeaderText = "Id";
            this.cId.MinimumWidth = 6;
            this.cId.Name = "cId";
            this.cId.ReadOnly = true;
            this.cId.Visible = false;
            this.cId.Width = 125;
            // 
            // cAerolinea
            // 
            this.cAerolinea.HeaderText = "Aerolinea";
            this.cAerolinea.MinimumWidth = 6;
            this.cAerolinea.Name = "cAerolinea";
            this.cAerolinea.ReadOnly = true;
            this.cAerolinea.Width = 125;
            // 
            // cPrecio
            // 
            this.cPrecio.HeaderText = "Precio";
            this.cPrecio.MinimumWidth = 6;
            this.cPrecio.Name = "cPrecio";
            this.cPrecio.ReadOnly = true;
            this.cPrecio.Width = 125;
            // 
            // cHSalida
            // 
            this.cHSalida.HeaderText = "Fecha Salida";
            this.cHSalida.MinimumWidth = 6;
            this.cHSalida.Name = "cHSalida";
            this.cHSalida.ReadOnly = true;
            this.cHSalida.Width = 125;
            // 
            // HoraSal
            // 
            this.HoraSal.HeaderText = "Hora Salida";
            this.HoraSal.Name = "HoraSal";
            this.HoraSal.ReadOnly = true;
            // 
            // cASalida
            // 
            this.cASalida.HeaderText = "Aeropuerto Salida";
            this.cASalida.MinimumWidth = 6;
            this.cASalida.Name = "cASalida";
            this.cASalida.ReadOnly = true;
            this.cASalida.Width = 125;
            // 
            // cHLlegada
            // 
            this.cHLlegada.HeaderText = "Fecha Regreso";
            this.cHLlegada.MinimumWidth = 6;
            this.cHLlegada.Name = "cHLlegada";
            this.cHLlegada.ReadOnly = true;
            this.cHLlegada.Width = 125;
            // 
            // HoraLLe
            // 
            this.HoraLLe.HeaderText = "Hora Regreso";
            this.HoraLLe.Name = "HoraLLe";
            this.HoraLLe.ReadOnly = true;
            // 
            // cALlegada
            // 
            this.cALlegada.HeaderText = "Aeropuerto Llegada";
            this.cALlegada.MinimumWidth = 6;
            this.cALlegada.Name = "cALlegada";
            this.cALlegada.ReadOnly = true;
            this.cALlegada.Width = 125;
            // 
            // cTiempoVuelo
            // 
            this.cTiempoVuelo.HeaderText = "Duracion Vuelo";
            this.cTiempoVuelo.MinimumWidth = 6;
            this.cTiempoVuelo.Name = "cTiempoVuelo";
            this.cTiempoVuelo.ReadOnly = true;
            this.cTiempoVuelo.Width = 125;
            // 
            // cAvion
            // 
            this.cAvion.HeaderText = "Avion";
            this.cAvion.MinimumWidth = 6;
            this.cAvion.Name = "cAvion";
            this.cAvion.ReadOnly = true;
            this.cAvion.Width = 125;
            // 
            // cPiloto1
            // 
            this.cPiloto1.HeaderText = "Piloto 1";
            this.cPiloto1.MinimumWidth = 6;
            this.cPiloto1.Name = "cPiloto1";
            this.cPiloto1.ReadOnly = true;
            this.cPiloto1.Width = 125;
            // 
            // cPiloto2
            // 
            this.cPiloto2.HeaderText = "Piloto 2";
            this.cPiloto2.MinimumWidth = 6;
            this.cPiloto2.Name = "cPiloto2";
            this.cPiloto2.ReadOnly = true;
            this.cPiloto2.Width = 125;
            // 
            // cTrip1
            // 
            this.cTrip1.HeaderText = "Tripulante 1";
            this.cTrip1.MinimumWidth = 6;
            this.cTrip1.Name = "cTrip1";
            this.cTrip1.ReadOnly = true;
            this.cTrip1.Width = 125;
            // 
            // cTrip2
            // 
            this.cTrip2.HeaderText = "Tripulante 2";
            this.cTrip2.MinimumWidth = 6;
            this.cTrip2.Name = "cTrip2";
            this.cTrip2.ReadOnly = true;
            this.cTrip2.Width = 125;
            // 
            // cTrip3
            // 
            this.cTrip3.HeaderText = "Tripulante 3";
            this.cTrip3.MinimumWidth = 6;
            this.cTrip3.Name = "cTrip3";
            this.cTrip3.ReadOnly = true;
            this.cTrip3.Width = 125;
            // 
            // tp2
            // 
            this.tp2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.tp2.Controls.Add(this.cbxAerolinea);
            this.tp2.Controls.Add(this.btnBVuelos);
            this.tp2.Controls.Add(this.label2);
            this.tp2.Controls.Add(this.dgvVAerolinea);
            this.tp2.Location = new System.Drawing.Point(4, 22);
            this.tp2.Margin = new System.Windows.Forms.Padding(2);
            this.tp2.Name = "tp2";
            this.tp2.Padding = new System.Windows.Forms.Padding(2);
            this.tp2.Size = new System.Drawing.Size(561, 376);
            this.tp2.TabIndex = 1;
            this.tp2.Text = "Vuelos por Aerolinea";
            // 
            // cbxAerolinea
            // 
            this.cbxAerolinea.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxAerolinea.FormattingEnabled = true;
            this.cbxAerolinea.Location = new System.Drawing.Point(105, 23);
            this.cbxAerolinea.Margin = new System.Windows.Forms.Padding(2);
            this.cbxAerolinea.Name = "cbxAerolinea";
            this.cbxAerolinea.Size = new System.Drawing.Size(147, 28);
            this.cbxAerolinea.TabIndex = 87;
            // 
            // btnBVuelos
            // 
            this.btnBVuelos.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBVuelos.FlatAppearance.BorderSize = 0;
            this.btnBVuelos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.btnBVuelos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBVuelos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBVuelos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBVuelos.ForeColor = System.Drawing.Color.White;
            this.btnBVuelos.Location = new System.Drawing.Point(403, 75);
            this.btnBVuelos.Name = "btnBVuelos";
            this.btnBVuelos.Size = new System.Drawing.Size(148, 46);
            this.btnBVuelos.TabIndex = 86;
            this.btnBVuelos.Text = "Buscar";
            this.btnBVuelos.UseVisualStyleBackColor = false;
            this.btnBVuelos.Click += new System.EventHandler(this.btnBVuelos_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(20, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 20);
            this.label2.TabIndex = 85;
            this.label2.Text = "Aerolineas:";
            // 
            // dgvVAerolinea
            // 
            this.dgvVAerolinea.AllowUserToAddRows = false;
            this.dgvVAerolinea.AllowUserToDeleteRows = false;
            this.dgvVAerolinea.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(91)))), ((int)(((byte)(149)))));
            this.dgvVAerolinea.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvVAerolinea.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.DarkSlateGray;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVAerolinea.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvVAerolinea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVAerolinea.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.HoraSalida,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.HoraLlegada,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.dgvVAerolinea.EnableHeadersVisualStyles = false;
            this.dgvVAerolinea.GridColor = System.Drawing.Color.DarkCyan;
            this.dgvVAerolinea.Location = new System.Drawing.Point(0, 139);
            this.dgvVAerolinea.Name = "dgvVAerolinea";
            this.dgvVAerolinea.ReadOnly = true;
            this.dgvVAerolinea.RowHeadersWidth = 51;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(91)))), ((int)(((byte)(149)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.DarkCyan;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White;
            this.dgvVAerolinea.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvVAerolinea.Size = new System.Drawing.Size(563, 241);
            this.dgvVAerolinea.TabIndex = 81;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Aerolinea";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 125;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Precio";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 125;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Fecha Salida";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 125;
            // 
            // HoraSalida
            // 
            this.HoraSalida.HeaderText = "Hora Salida";
            this.HoraSalida.Name = "HoraSalida";
            this.HoraSalida.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Aeropuerto Salida";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 125;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Fecha Regreso";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 125;
            // 
            // HoraLlegada
            // 
            this.HoraLlegada.HeaderText = "Hora Regreso";
            this.HoraLlegada.Name = "HoraLlegada";
            this.HoraLlegada.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Aeropuerto Llegada";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 125;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "Duracion Vuelo";
            this.dataGridViewTextBoxColumn14.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 125;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Avion";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 125;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Piloto 1";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 125;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Piloto 2";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 125;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Tripulante 1";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 125;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "Tripulante 2";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 125;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "Tripulante 3";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 125;
            // 
            // tp3
            // 
            this.tp3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.tp3.Controls.Add(this.dgvAviones);
            this.tp3.Location = new System.Drawing.Point(4, 22);
            this.tp3.Margin = new System.Windows.Forms.Padding(2);
            this.tp3.Name = "tp3";
            this.tp3.Padding = new System.Windows.Forms.Padding(2);
            this.tp3.Size = new System.Drawing.Size(561, 376);
            this.tp3.TabIndex = 2;
            this.tp3.Text = "Ranking de Aviones";
            // 
            // dgvAviones
            // 
            this.dgvAviones.AllowUserToAddRows = false;
            this.dgvAviones.AllowUserToDeleteRows = false;
            this.dgvAviones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAviones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvAviones.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(91)))), ((int)(((byte)(149)))));
            this.dgvAviones.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvAviones.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.DarkSlateGray;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAviones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvAviones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAviones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15});
            this.dgvAviones.EnableHeadersVisualStyles = false;
            this.dgvAviones.GridColor = System.Drawing.Color.DarkCyan;
            this.dgvAviones.Location = new System.Drawing.Point(0, 6);
            this.dgvAviones.Name = "dgvAviones";
            this.dgvAviones.ReadOnly = true;
            this.dgvAviones.RowHeadersWidth = 51;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(91)))), ((int)(((byte)(149)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.DarkCyan;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.White;
            this.dgvAviones.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvAviones.Size = new System.Drawing.Size(563, 375);
            this.dgvAviones.TabIndex = 41;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "Lista de aviones";
            this.dataGridViewTextBoxColumn15.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 126;
            // 
            // tp4
            // 
            this.tp4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.tp4.Controls.Add(this.btnBTrip);
            this.tp4.Controls.Add(this.cbxAerolineas2);
            this.tp4.Controls.Add(this.label5);
            this.tp4.Controls.Add(this.dgvTripulantes);
            this.tp4.Location = new System.Drawing.Point(4, 22);
            this.tp4.Margin = new System.Windows.Forms.Padding(2);
            this.tp4.Name = "tp4";
            this.tp4.Padding = new System.Windows.Forms.Padding(2);
            this.tp4.Size = new System.Drawing.Size(561, 376);
            this.tp4.TabIndex = 3;
            this.tp4.Text = "Tripulación por Aerolinea";
            // 
            // btnBTrip
            // 
            this.btnBTrip.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBTrip.FlatAppearance.BorderSize = 0;
            this.btnBTrip.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.btnBTrip.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBTrip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBTrip.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBTrip.ForeColor = System.Drawing.Color.White;
            this.btnBTrip.Location = new System.Drawing.Point(399, 80);
            this.btnBTrip.Name = "btnBTrip";
            this.btnBTrip.Size = new System.Drawing.Size(148, 46);
            this.btnBTrip.TabIndex = 36;
            this.btnBTrip.Text = "Buscar";
            this.btnBTrip.UseVisualStyleBackColor = false;
            this.btnBTrip.Click += new System.EventHandler(this.btnBTrip_Click);
            // 
            // cbxAerolineas2
            // 
            this.cbxAerolineas2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxAerolineas2.FormattingEnabled = true;
            this.cbxAerolineas2.Location = new System.Drawing.Point(107, 22);
            this.cbxAerolineas2.Name = "cbxAerolineas2";
            this.cbxAerolineas2.Size = new System.Drawing.Size(191, 28);
            this.cbxAerolineas2.TabIndex = 35;
            this.cbxAerolineas2.Text = "Seleccione";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(13, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 20);
            this.label5.TabIndex = 34;
            this.label5.Text = "Aerolinea:";
            // 
            // dgvTripulantes
            // 
            this.dgvTripulantes.AllowUserToAddRows = false;
            this.dgvTripulantes.AllowUserToDeleteRows = false;
            this.dgvTripulantes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(91)))), ((int)(((byte)(149)))));
            this.dgvTripulantes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvTripulantes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.DarkSlateGray;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTripulantes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvTripulantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTripulantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this.cCedula,
            this.cNombre,
            this.cFechaN,
            this.dataGridViewTextBoxColumn18,
            this.cRol,
            this.dataGridViewTextBoxColumn19});
            this.dgvTripulantes.EnableHeadersVisualStyles = false;
            this.dgvTripulantes.GridColor = System.Drawing.Color.DarkCyan;
            this.dgvTripulantes.Location = new System.Drawing.Point(-2, 141);
            this.dgvTripulantes.Name = "dgvTripulantes";
            this.dgvTripulantes.ReadOnly = true;
            this.dgvTripulantes.RowHeadersWidth = 51;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(91)))), ((int)(((byte)(149)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.DarkCyan;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.White;
            this.dgvTripulantes.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvTripulantes.Size = new System.Drawing.Size(566, 241);
            this.dgvTripulantes.TabIndex = 27;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "Id";
            this.dataGridViewTextBoxColumn17.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Visible = false;
            this.dataGridViewTextBoxColumn17.Width = 125;
            // 
            // cCedula
            // 
            this.cCedula.HeaderText = "Cedula";
            this.cCedula.MinimumWidth = 6;
            this.cCedula.Name = "cCedula";
            this.cCedula.ReadOnly = true;
            this.cCedula.Width = 125;
            // 
            // cNombre
            // 
            this.cNombre.HeaderText = "Nombre";
            this.cNombre.MinimumWidth = 6;
            this.cNombre.Name = "cNombre";
            this.cNombre.ReadOnly = true;
            this.cNombre.Width = 125;
            // 
            // cFechaN
            // 
            this.cFechaN.HeaderText = "Fecha Nacimiento";
            this.cFechaN.MinimumWidth = 6;
            this.cFechaN.Name = "cFechaN";
            this.cFechaN.ReadOnly = true;
            this.cFechaN.Width = 125;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "Aerolinea";
            this.dataGridViewTextBoxColumn18.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 125;
            // 
            // cRol
            // 
            this.cRol.HeaderText = "Rol";
            this.cRol.MinimumWidth = 6;
            this.cRol.Name = "cRol";
            this.cRol.ReadOnly = true;
            this.cRol.Width = 150;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "Estado";
            this.dataGridViewTextBoxColumn19.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 125;
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Image = global::Aplicacion.Properties.Resources.icons8_subtract_32px;
            this.btnMinimizar.Location = new System.Drawing.Point(743, 12);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(15, 15);
            this.btnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimizar.TabIndex = 16;
            this.btnMinimizar.TabStop = false;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Image = global::Aplicacion.Properties.Resources.icons8_delete_32px;
            this.btnCerrar.Location = new System.Drawing.Point(773, 12);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(15, 15);
            this.btnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnCerrar.TabIndex = 15;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(222, 54);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 363);
            this.panel2.TabIndex = 18;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(781, 54);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 363);
            this.panel3.TabIndex = 19;
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(713, 50);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(78, 10);
            this.panel4.TabIndex = 20;
            // 
            // FrmReportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.ClientSize = new System.Drawing.Size(800, 445);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnMinimizar);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmReportes";
            this.Opacity = 0.98D;
            this.Text = "FrmAeropuertos";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmAeropuertos_MouseDown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tp1.ResumeLayout(false);
            this.tp1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVFechas)).EndInit();
            this.tp2.ResumeLayout(false);
            this.tp2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVAerolinea)).EndInit();
            this.tp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAviones)).EndInit();
            this.tp4.ResumeLayout(false);
            this.tp4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTripulantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btnMinimizar;
        private System.Windows.Forms.PictureBox btnCerrar;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tp1;
        private System.Windows.Forms.TabPage tp2;
        private System.Windows.Forms.DateTimePicker dtpFecha2;
        private System.Windows.Forms.DateTimePicker dtpFecha1;
        private System.Windows.Forms.DataGridView dgvVFechas;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBFecha;
        private System.Windows.Forms.ComboBox cbxAerolinea;
        private System.Windows.Forms.Button btnBVuelos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvVAerolinea;
        private System.Windows.Forms.TabPage tp3;
        private System.Windows.Forms.DataGridView dgvAviones;
        private System.Windows.Forms.TabPage tp4;
        private System.Windows.Forms.DataGridView dgvTripulantes;
        private System.Windows.Forms.ComboBox cbxAerolineas2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnBTrip;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFechaN;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn cRol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn cId;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAerolinea;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPrecio;
        private System.Windows.Forms.DataGridViewTextBoxColumn cHSalida;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraSal;
        private System.Windows.Forms.DataGridViewTextBoxColumn cASalida;
        private System.Windows.Forms.DataGridViewTextBoxColumn cHLlegada;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraLLe;
        private System.Windows.Forms.DataGridViewTextBoxColumn cALlegada;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTiempoVuelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAvion;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPiloto1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPiloto2;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTrip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTrip2;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTrip3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraSalida;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraLlegada;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
    }
}
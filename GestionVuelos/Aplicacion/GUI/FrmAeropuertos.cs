﻿using Aplicacion.BOL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.GUI
{
    public partial class FrmAeropuertos : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        public FrmAeropuertos()
        {
            InitializeComponent();
            CenterToScreen();
            MostrarAeropuertos();
        }

        private void FrmAeropuertos_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        /// <summary>
        /// Muestra mensajes al usuario
        /// </summary>
        /// <param name="msg"></param> mensaje que genera la aplicacion en caso de error o eventualidad
        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Limpiar el formulario
        /// </summary>
        private void limpiar()
        {
            txtIATA.Clear();
            txtPais.Clear();
            txtNombre.Clear();
            lblMensaje.Visible = false;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Muestra los aeropuertos
        /// </summary>
        public void MostrarAeropuertos()
        {
            dgvAeropuertos.Rows.Clear();
            foreach (Aeropuerto a in new AeropuertoBOL().CargarAeropuertos())
            {
                dgvAeropuertos.Rows.Add(a.id, a.iata, a.nombre, a.pais);

            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtIATA.Text != "" || txtNombre.Text != "" || txtPais.Text != "")
            {
                Aeropuerto a = new Aeropuerto();
                a.iata = txtIATA.Text.Trim();
                a.nombre = txtNombre.Text.Trim();
                a.pais = txtPais.Text.Trim();
                new AeropuertoBOL().Insertar(a);
                MostrarAeropuertos();
                MessageBox.Show("Aeropuerto registrado", "Información");
                limpiar();
            }
            else
            {
                msgError("Debe escribir un nombre, \n un país y el codigo IATA");
            }
        }
    }
}

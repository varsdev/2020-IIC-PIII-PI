﻿using Aplicacion.BOL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.GUI
{
    public partial class FrmAviones : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        public FrmAviones()
        {
            InitializeComponent();
            CenterToScreen();
            cbxAerolinea.DataSource = new AerolineaBOL().CargarAerolineas();
            dtpConstruccion.ShowUpDown = true;
            MostrarAviones();
        }

        /// <summary>
        /// Muestra los aviones
        /// </summary>
        public void MostrarAviones()
        {
            dgvAviones.Rows.Clear();
            foreach (Avion a in new AvionBOL().CargarAviones())
            {
                dgvAviones.Rows.Add(a.id, a.modelo, a.annoConstruccion, a.aerolinea, a.capacidad,
                    a.estado==true?"Activo":"Inactivo");

            }
        }

        /// <summary>
        /// Muestra mensajes al usuario
        /// </summary>
        /// <param name="msg"></param> mensaje que genera la aplicacion en caso de error o eventualidad
        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Limpia el formulario
        /// </summary>
        private void limpiar()
        {
            txtModelo.Clear();
            txtCapacidad.Clear();
            dtpConstruccion.ResetText();
            cbxAerolinea.Text = "Seleccione";
            lblMensaje.Visible = false;
        }

        private void FrmAviones_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtModelo.Text != "" || txtCapacidad.Text !=""){
                Avion a = new Avion();
                a.modelo = txtModelo.Text.Trim();
                a.annoConstruccion = dtpConstruccion.Value.Year.ToString();
                a.aerolinea = (Aerolinea)cbxAerolinea.SelectedItem;
                a.capacidad = txtCapacidad.Text.Trim();
                a.estado = true;
                new AvionBOL().Insertar(a);
                MostrarAviones();
                MessageBox.Show("Avion registrado", "Información");
                limpiar();
            }
            else
            {
                msgError("Debe escribir un modelo y una capacidad");
            }
            
        }

        private void txtCapacidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

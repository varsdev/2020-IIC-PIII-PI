CREATE DATABASE gestionvuelo;

CREATE TABLE aerolineas
(
	id serial PRIMARY KEY,
	nombre text NOT NULL,
	anno_fundacion text NOT NULL,
	tipo text NOT NULL,
	CONSTRAINT unq_aerolineas_nombre UNIQUE (nombre)
);

CREATE TABLE tripulantes 
(
	id serial PRIMARY KEY,
	cedula text NOT NULL,
	nombre text NOT NULL,
	fecha_nacimiento date NOT NULL,
	id_aerolinea int NOT NULL,
	rol text NOT NULL,
	estado boolean DEFAULT true,
	CONSTRAINT unq_tripulantes_cedula UNIQUE (cedula),
	CONSTRAINT fk_tri_aeroli FOREIGN KEY(id_aerolinea) REFERENCES aerolineas(id)
);

CREATE TABLE aviones
(
	id serial PRIMARY KEY,
	modelo text NOT NULL,
	anno_construccion text NOT NULL,
	id_aerolinea int NOT NULL,
	capacidad text NOT NULL,
	estado boolean DEFAULT true,
	CONSTRAINT fk_avio_aeroli FOREIGN KEY(id_aerolinea) REFERENCES aerolineas(id)
);

CREATE TABLE aeropuertos
(
	id serial PRIMARY KEY,
	iata text NOT NULL,
	nombre text NOT NULL,
	pais text NOT NULL,
	CONSTRAINT unq_aeropuertos_iata UNIQUE (iata)
);

CREATE TABLE vuelos
(
	id serial PRIMARY KEY,
	id_aerolinea int NOT NULL,
	precio numeric DEFAULT 0,
	fecha_salida date NOT NULL,
	hora_salida time NOT NULL,
	id_aeropuerto_salida int NOT NULL,
	fecha_llegada date NOT NULL,
	hora_llegada time NOT NULL,
	id_aeropuerto_llegada int NOT NULL,
	id_avion int NOT NULL,
	id_piloto1 int NOT NULL,
	id_piloto2 int NOT NULL,
	id_tripulante1 int NOT NULL,
	id_tripulante2 int NOT NULL,
	id_tripulante3 int NOT NULL,
	tiempo_vuelo time not null,
	CONSTRAINT fk_vue_aeroli FOREIGN KEY(id_aerolinea) REFERENCES aerolineas(id),
	CONSTRAINT fk_vue_aeropuesal FOREIGN KEY(id_aeropuerto_salida) REFERENCES aeropuertos(id),
	CONSTRAINT fk_vue_aeropuelleg FOREIGN KEY(id_aeropuerto_llegada) REFERENCES aeropuertos(id),
	CONSTRAINT fk_vue_avion FOREIGN KEY(id_avion) REFERENCES aviones(id),
	CONSTRAINT fk_vue_pilo1 FOREIGN KEY(id_piloto1) REFERENCES tripulantes(id),
	CONSTRAINT fk_vue_pilo2 FOREIGN KEY(id_piloto2) REFERENCES tripulantes(id),
	CONSTRAINT fk_vue_tripu1 FOREIGN KEY(id_tripulante1) REFERENCES tripulantes(id),
	CONSTRAINT fk_vue_tripu2 FOREIGN KEY(id_tripulante2) REFERENCES tripulantes(id),
	CONSTRAINT fk_vue_tripu3 FOREIGN KEY(id_tripulante3) REFERENCES tripulantes(id)
);

CREATE TABLE usuarios
( 
	id serial PRIMARY KEY,
	cedula text NOT NULL,
	nombre text NOT NULL,
	edad int,
	contrasenna text NOT NULL,
	tipo text NOT NULL,
	CONSTRAINT unq_usuarios_cedula UNIQUE (cedula)
);

CREATE TABLE historiales
(
	id serial PRIMARY KEY,
	cedula text NOT NULL,
	nombre_pais_salida text NOT NULL,
	nombre_pais_llegada text NOT NULL,
	nombre_pais_escala1 text,
	nombre_pais_escala2 text,
	fecha_compra_vuelo date NOT NULL,
	hora_compra_vuelo time NOT NULL,
	tiempo_vuelo time NOT NULL,
	costo_total numeric DEFAULT 0
);

select * from usuarios
select * from tripulantes
select * from aeropuertos
select * from aerolineas
select * from aviones
select * from vuelos
select * from historiales
 
--USUARIOS contrasenna = qweasd
INSERT INTO usuarios (cedula, nombre, edad, contrasenna, tipo) 
VALUES ('Admin', 'Luis Guerrero', 0, '36f17c3939ac3e7b2fc9396fa8e953ea', 'Administrador');
INSERT INTO usuarios (cedula, nombre, edad, contrasenna, tipo) 
VALUES ('123', 'Carlos Vargas', 21, '36f17c3939ac3e7b2fc9396fa8e953ea', 'Pasajero');
INSERT INTO usuarios (cedula, nombre, edad, contrasenna, tipo) 
VALUES ('456', 'Silvana Alvarado', 24, '36f17c3939ac3e7b2fc9396fa8e953ea', 'Pasajero');
INSERT INTO usuarios (cedula, nombre, edad, contrasenna, tipo) 
VALUES ('789', 'Keylor Soto', 23, '36f17c3939ac3e7b2fc9396fa8e953ea', 'Pasajero');
INSERT INTO usuarios (cedula, nombre, edad, contrasenna, tipo) 
VALUES ('321', 'Andrea Vargas', 20, '36f17c3939ac3e7b2fc9396fa8e953ea', 'Pasajero');

--AEROLINEAS
INSERT INTO aerolineas (nombre, anno_fundacion, tipo)
VALUES ('Emirates', '1995', 'Internacional');
INSERT INTO aerolineas (nombre, anno_fundacion, tipo)
VALUES ('Qatar', '1990', 'Internacional');

--AVIONES EMIRATES
INSERT INTO aviones (modelo, anno_construccion, id_aerolinea, capacidad, estado)
VALUES ('Boeing 747', '2000', 1, '240', true);
INSERT INTO aviones (modelo, anno_construccion, id_aerolinea, capacidad, estado)
VALUES ('Arbus A320', '2010', 1, '200', true);
INSERT INTO aviones (modelo, anno_construccion, id_aerolinea, capacidad, estado)
VALUES ('Boing-c17', '2015', 1, '220', true);
INSERT INTO aviones (modelo, anno_construccion, id_aerolinea, capacidad, estado)
VALUES ('Boeing 747', '2016', 1, '270', true);

--AVIONES Qatar
INSERT INTO aviones (modelo, anno_construccion, id_aerolinea, capacidad, estado)
VALUES ('Boeing 747', '2005', 2, '250', true);
INSERT INTO aviones (modelo, anno_construccion, id_aerolinea, capacidad, estado)
VALUES ('Arbus A320', '2012', 2, '210', true);
INSERT INTO aviones (modelo, anno_construccion, id_aerolinea, capacidad, estado)
VALUES ('Boing-c17', '2010', 2, '200', true);
INSERT INTO aviones (modelo, anno_construccion, id_aerolinea, capacidad, estado)
VALUES ('Boeing 747', '2018', 2, '300', true);

--AEROPUERTOS 
INSERT INTO aeropuertos (iata, nombre, pais)
VALUES ('SJO', 'Aeropuerto Juan Santamaria', 'Costa Rica');
INSERT INTO aeropuertos (iata, nombre, pais)
VALUES ('PHL', 'Aeropuerto de Filadelfia', 'USA');
INSERT INTO aeropuertos (iata, nombre, pais)
VALUES ('MAD', 'Aeropuerto de Madrid-Barajas', 'España');
INSERT INTO aeropuertos (iata, nombre, pais)
VALUES ('MIA', 'Aeropuerto de Miami', 'USA');
INSERT INTO aeropuertos (iata, nombre, pais)
VALUES ('MCO', 'Aeropuerto de Orlando', 'USA');
INSERT INTO aeropuertos (iata, nombre, pais)
VALUES ('LAS', 'Aeropuerto Las Vegas', 'USA');
INSERT INTO aeropuertos (iata, nombre, pais)
VALUES ('LAX', 'Aeropuerto Los Angeles', 'USA');
INSERT INTO aeropuertos (iata, nombre, pais)
VALUES ('IBZ', 'Aeropuerto de Ibiza', 'España');
INSERT INTO aeropuertos (iata, nombre, pais)
VALUES ('GRU', 'Aeropuerto Sao Paolo', 'Brasil');
INSERT INTO aeropuertos (iata, nombre, pais)
VALUES ('MEX', 'Aeropuerto Benito Juarez', 'Mexico');

--TRIPULANTES
--Servicio al Cliente Emirates
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('1', 'Hazel Campos', '12/07/1998', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('2', 'Hazel Solis', '12/07/1999', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('3', 'Pamela Campos', '12/07/1997', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('4', 'Pamela Quiros', '10/08/2000', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('5', 'Andrea Rojas', '12/09/2001', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('6', 'Mariela Rojas', '12/10/2000', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('7', 'Natalia Rojas', '12/09/2001', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('8', 'Sileny Araya', '12/09/2000', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('9', 'Noelia Rojas', '12/09/2001', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('10', 'Yendry Araya', '12/09/1999', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('11', 'Juan Rojas', '12/09/1999', 1, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('12', 'Marcos Araya', '12/09/2000', 1, 'Servicio al Cliente', true);

--Pilotos Emirates
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('13', 'Daniel Rojas', '12/07/1998', 1, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('14', 'Carlos Montero', '12/07/1999', 1, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('15', 'Victor Fernandez', '12/07/1997', 1, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('16', 'Priscilla Rojas', '10/08/2000', 1, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('17', 'Antonio Martinez', '12/09/2001', 1, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('18', 'Gipzy Rodriguez', '12/10/2000', 1, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('19', 'Alberto Maradiaga', '12/09/2001', 1, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('20', 'Beatriz Rojas', '12/09/2000', 1, 'Piloto', true);

--Pilotos Qatar
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('21', 'Fernando Rojas', '12/07/1998', 2, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('22', 'Andre Montero', '12/07/1999', 2, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('23', 'Samuel Fernandez', '12/07/1997', 2, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('24', 'Esther Rojas', '10/08/2000', 2, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('25', 'Victoria Martinez', '12/09/2001', 2, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('26', 'Miguel Rodriguez', '12/10/2000', 2, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('27', 'Silvana Maradiaga', '12/09/2001', 2, 'Piloto', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('28', 'Micaela Rojas', '12/09/2000', 2, 'Piloto', true);

--Servicio al Cliente Qatar
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('29', 'Yoel Campos', '12/07/1998', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('30', 'Ronaldo Solis', '12/07/1999', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('31', 'Keylor Campos', '12/07/1997', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('32', 'Antonia Quiros', '10/08/2000', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('33', 'Vanessa Rojas', '12/09/2001', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('34', 'Paola Rojas', '12/10/2000', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('35', 'Paula Rojas', '12/09/2001', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('36', 'Karla Araya', '12/09/2000', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('37', 'Xiomara Rojas', '12/09/2001', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('38', 'Zoe Araya', '12/09/1999', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('39', 'Kembly Rojas', '12/09/1999', 2, 'Servicio al Cliente', true);
INSERT INTO tripulantes (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
VALUES ('40', 'Wendy Araya', '12/09/2000', 2, 'Servicio al Cliente', true);

--Vuelos
INSERT INTO public.vuelos(id_aerolinea, precio, fecha_salida, hora_salida, id_aeropuerto_salida,
fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, id_piloto1, id_piloto2, id_tripulante1, 
id_tripulante2, id_tripulante3, tiempo_vuelo)
VALUES (1 , 125000, '15/08/2020', '10:00:00', 1, '20/08/2020', '14:30:00', 4, 1, 13, 14, 1, 2, 3, '03:00:00');
INSERT INTO public.vuelos(id_aerolinea, precio, fecha_salida, hora_salida, id_aeropuerto_salida,
fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, id_piloto1, id_piloto2, id_tripulante1, 
id_tripulante2, id_tripulante3, tiempo_vuelo)
VALUES (1 , 110000, '15/08/2020', '14:00:00', 4, '20/08/2020', '10:45:00', 3, 2, 15, 16, 4, 5, 6, '05:45:00');
INSERT INTO public.vuelos(id_aerolinea, precio, fecha_salida, hora_salida, id_aeropuerto_salida,
fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, id_piloto1, id_piloto2, id_tripulante1, 
id_tripulante2, id_tripulante3, tiempo_vuelo)
VALUES (1 , 65000, '18/08/2020', '08:40:00', 4, '28/08/2020', '13:20:00', 5, 3, 17, 18, 7, 8, 9, '01:20:00');
INSERT INTO public.vuelos(id_aerolinea, precio, fecha_salida, hora_salida, id_aeropuerto_salida,
fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, id_piloto1, id_piloto2, id_tripulante1, 
id_tripulante2, id_tripulante3, tiempo_vuelo)
VALUES (1 , 97000, '15/08/2020', '20:30:00', 3, '20/08/2020', '08:10:00', 8, 4, 19, 20, 10, 11, 12, '01:20:00');
INSERT INTO public.vuelos(id_aerolinea, precio, fecha_salida, hora_salida, id_aeropuerto_salida,
fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, id_piloto1, id_piloto2, id_tripulante1, 
id_tripulante2, id_tripulante3, tiempo_vuelo)
VALUES (2 , 75000, '21/08/2020', '10:30:00', 6, '26/08/2020', '14:30:00', 7, 5, 21, 22, 29, 30, 31, '03:00:00');
INSERT INTO public.vuelos(id_aerolinea, precio, fecha_salida, hora_salida, id_aeropuerto_salida,
fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, id_piloto1, id_piloto2, id_tripulante1, 
id_tripulante2, id_tripulante3, tiempo_vuelo)
VALUES (2 , 175000, '21/08/2020', '14:00:00', 7, '26/08/2020', '07:30:00', 10, 6, 23, 24, 32, 33, 34, '02:30:00');
INSERT INTO public.vuelos(id_aerolinea, precio, fecha_salida, hora_salida, id_aeropuerto_salida,
fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, id_piloto1, id_piloto2, id_tripulante1, 
id_tripulante2, id_tripulante3, tiempo_vuelo)
VALUES (2 , 145000, '15/08/2020', '20:00:00', 3, '20/08/2020', '15:00:00', 8, 7, 25, 26, 35, 36, 37, '01:40:00');
INSERT INTO public.vuelos(id_aerolinea, precio, fecha_salida, hora_salida, id_aeropuerto_salida,
fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, id_piloto1, id_piloto2, id_tripulante1, 
id_tripulante2, id_tripulante3, tiempo_vuelo)
VALUES (2 , 120000, '21/08/2020', '18:00:00', 10, '26/08/2020', '10:00:00', 9, 8, 27, 28, 38, 39, 40, '06:00:00');

--Historiales
INSERT INTO historiales (cedula, nombre_pais_salida, nombre_pais_llegada, nombre_pais_escala1,
nombre_pais_escala2, fecha_compra_vuelo, hora_compra_vuelo, tiempo_vuelo, costo_total) 
VALUES (123, 'Costa Rica', 'USA', 'Sin escala', 'Sin escala', '28/07/2020', '10:30:00', '03:00:00', 125000);
INSERT INTO historiales (cedula, nombre_pais_salida, nombre_pais_llegada, nombre_pais_escala1,
nombre_pais_escala2, fecha_compra_vuelo, hora_compra_vuelo, tiempo_vuelo, costo_total) 
VALUES (456, 'Costa Rica', 'España', 'USA', 'Sin escala', '30/07/2020', '11:45:00', '08:45:00', 235000);
INSERT INTO historiales (cedula, nombre_pais_salida, nombre_pais_llegada, nombre_pais_escala1,
nombre_pais_escala2, fecha_compra_vuelo, hora_compra_vuelo, tiempo_vuelo, costo_total) 
VALUES (789, 'USA', 'USA', 'Sin escala', 'Sin escala', '31/07/2020', '09:30:00', '01:20:00', 65000);
INSERT INTO historiales (cedula, nombre_pais_salida, nombre_pais_llegada, nombre_pais_escala1,
nombre_pais_escala2, fecha_compra_vuelo, hora_compra_vuelo, tiempo_vuelo, costo_total) 
VALUES (321, 'España', 'España', 'Sin escala', 'Sin escala', '01/08/2020', '14:45:00', '01:40:00', 145000);
INSERT INTO historiales (cedula, nombre_pais_salida, nombre_pais_llegada, nombre_pais_escala1,
nombre_pais_escala2, fecha_compra_vuelo, hora_compra_vuelo, tiempo_vuelo, costo_total) 
VALUES (123, 'USA', 'Mexico', 'USA', 'Sin escala', '01/08/2020', '16:00:00', '05:30:00', 250000);
INSERT INTO historiales (cedula, nombre_pais_salida, nombre_pais_llegada, nombre_pais_escala1,
nombre_pais_escala2, fecha_compra_vuelo, hora_compra_vuelo, tiempo_vuelo, costo_total) 
VALUES (456, 'USA', 'USA', 'Sin escala', 'Sin escala', '02/08/2020', '16:00:00', '03:00:00', 75000);
INSERT INTO historiales (cedula, nombre_pais_salida, nombre_pais_llegada, nombre_pais_escala1,
nombre_pais_escala2, fecha_compra_vuelo, hora_compra_vuelo, tiempo_vuelo, costo_total) 
VALUES (321, 'Costa Rica', 'España', 'USA', 'España', '01/08/2020', '16:00:00', '10:05:00', 332000);
INSERT INTO historiales (cedula, nombre_pais_salida, nombre_pais_llegada, nombre_pais_escala1,
nombre_pais_escala2, fecha_compra_vuelo, hora_compra_vuelo, tiempo_vuelo, costo_total) 
VALUES (789, 'USA', 'Brasil', 'USA', 'Mexico', '04/08/2020', '12:45:00', '11:30:00', 370000);
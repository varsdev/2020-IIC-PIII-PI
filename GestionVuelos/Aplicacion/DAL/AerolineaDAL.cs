﻿using Aplicacion.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.DAL
{
    class AerolineaDAL
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Inserta a la BD
        /// </summary>
        /// <param name="a"></param> aerolinea
        public void InsertarDatos(Aerolinea a)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO aerolineas (nombre, anno_fundacion, tipo) VALUES ('" + a.nombre + "', '" + a.annoFundacion + "', '" + a.tipo + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Carga lista de aerolineas
        /// </summary>
        /// <returns></returns> una lista
        public List<Aerolinea> ConsultarDatos()
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Aerolinea> lista = new List<Aerolinea>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre, anno_fundacion, tipo FROM aerolineas", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Crea una aerolinea con los datos de la BD
        /// </summary>
        /// <param name="dr"></param> datos de la BD
        /// <returns></returns>
        private Aerolinea Cargar(NpgsqlDataReader dr)
        {
            Aerolinea a = new Aerolinea()
            {
                id = dr.GetInt32(0),
                nombre = dr.GetString(1),
                annoFundacion = dr.GetString(2),
                tipo = dr.GetString(3),
            };
            return a;
        }

        /// <summary>
        /// Carga aerolinea especifica
        /// </summary>
        /// <param name="id"></param> filtro de id
        /// <returns></returns> aerolinea
        public Aerolinea CargarAT(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, a.nombre, anno_fundacion, tipo FROM aerolineas a " +
                " inner join tripulantes t on t.id_aerolinea = a.id " +
                " WHERE t.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Aerolinea aerolinea = new Aerolinea();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    aerolinea = Cargar(dr);
                    conexion.Close();
                    return aerolinea;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga una aerolinea especifica
        /// </summary>
        /// <param name="id"></param> filtro de id
        /// <returns></returns> aerolinea
        public Aerolinea CargarAA(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, a.nombre, anno_fundacion, tipo FROM aerolineas a " +
                " right join aviones b on b.id_aerolinea = a.id " +
                " WHERE b.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Aerolinea aerolinea = new Aerolinea();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    aerolinea = Cargar(dr);
                    conexion.Close();
                    return aerolinea;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga una aerolinea especifica
        /// </summary>
        /// <param name="id"></param> filtro de id
        /// <returns></returns> aerolinea
        public Aerolinea CargarAV(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, a.nombre, anno_fundacion, tipo FROM aerolineas a " +
                " right join vuelos v on v.id_aerolinea = a.id " +
                " WHERE v.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Aerolinea aerolinea = new Aerolinea();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    aerolinea = Cargar(dr);
                    conexion.Close();
                    return aerolinea;
                }
            }
            conexion.Close();
            return null;
        }


    }
}

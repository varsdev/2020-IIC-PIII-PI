﻿using Aplicacion.Entities;
using Microsoft.SqlServer.Server;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.DAL
{
    class VueloDAL
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Inserta a la Bd
        /// </summary>
        /// <param name="v"></param> vuelo
        public void InsertarDatos(Vuelo v)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO public.vuelos(id_aerolinea, precio, fecha_salida, hora_salida, " +
                " id_aeropuerto_salida, fecha_llegada, hora_llegada, id_aeropuerto_llegada, " +
                " id_avion, id_piloto1, id_piloto2, id_tripulante1, id_tripulante2, " +
                " id_tripulante3, tiempo_vuelo) VALUES ('" + v.aerolinea.id + "', '" + v.precio + "', '" + 
                v.fechaSalida + "', '" + v.horaSalida + "', '" + v.aeropuertoSalida.id + "', '" + v.fechaLlegada 
                + "', '" + v.horaLlegada + "', '" + v.aeropuertoLlegada.id+ "', '" + v.avion.id+ "', '" + v.piloto1.id
                + "', '" + v.piloto2.id + "', '" + v.tripulante1.id+ "', '" + v.tripulante2.id+
                "', '" + v.tripulante3.id + "', '" + v.tiempoVuelo+ "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Carga lista de vuelos 
        /// </summary>
        /// <returns></returns> lista
        public List<Vuelo> ConsultarDatos()
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Vuelo> lista = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, id_aerolinea, precio, fecha_salida, hora_salida, " +
                " id_aeropuerto_salida, fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, " +
                " id_piloto1, id_piloto2, id_tripulante1, id_tripulante2, id_tripulante3, tiempo_vuelo " +
                " FROM public.vuelos ORDER BY id_aerolinea", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Crea un vuelo con los datos de la BD
        /// </summary>
        /// <param name="dr"></param> datos
        /// <returns></returns> vuelo
        private Vuelo Cargar(NpgsqlDataReader dr)
        {
            Vuelo v = new Vuelo()
            {
                id = dr.GetInt32(0),
                aerolinea = new AerolineaDAL().CargarAV(dr.GetInt32(0)),
                precio = dr.GetDouble(2),
                fechaSalida = dr.GetDateTime(3),
                horaSalida = dr.GetTimeSpan(4),
                aeropuertoSalida = new AeropuertoDAL().CargarAeV1(dr.GetInt32(0)),
                fechaLlegada = dr.GetDateTime(6),
                horaLlegada = dr.GetTimeSpan(7),
                aeropuertoLlegada = new AeropuertoDAL().CargarAeV2(dr.GetInt32(0)),
                avion = new AvionDAL().CargarAvV(dr.GetInt32(0)),
                piloto1 = new TripulanteDAL().CargarP1(dr.GetInt32(0)),
                piloto2 = new TripulanteDAL().CargarP2(dr.GetInt32(0)),
                tripulante1 = new TripulanteDAL().CargarT1(dr.GetInt32(0)),
                tripulante2 = new TripulanteDAL().CargarT2(dr.GetInt32(0)),
                tripulante3 = new TripulanteDAL().CargarT3(dr.GetInt32(0)),
                tiempoVuelo = dr.GetTimeSpan(15),
            };
            return v;
        }

        /// <summary>
        /// Carga reporte de vuelos 
        /// </summary>
        /// <param name="fe1"></param> filtro de fecha 1
        /// <param name="fe2"></param> filtro de fecha 2
        /// <returns></returns> lista
        public List<Vuelo> CargarReporte(string fe1,string fe2)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Vuelo> lista = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, id_aerolinea, precio, fecha_salida, hora_salida, " +
                " id_aeropuerto_salida, fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, " +
                " id_piloto1, id_piloto2, id_tripulante1, id_tripulante2, id_tripulante3, tiempo_vuelo " +
                " FROM public.vuelos WHERE fecha_salida BETWEEN CAST(@f1 AS timestamp) AND CAST" +
                "(@f2 AS timestamp) or fecha_llegada BETWEEN CAST(@f1 AS timestamp) " +
                "AND CAST(@f2 AS timestamp)", conexion);
            cmd.Parameters.AddWithValue("@f1", fe1 + '%');
            cmd.Parameters.AddWithValue("@f2", fe2 + '%');
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga vuelos normales para comprar
        /// </summary>
        /// <param name="f1"></param> filtro de fecha salida
        /// <param name="f2"></param> filtro de fecha regreso
        /// <param name="id_aero1"></param> filtro de aeropuerto salida
        /// <param name="id_aero2"></param> filtro de aeropuerto regreso
        /// <returns></returns> lista
        public List<Vuelo> CargarVuelosNormal(string f1, string f2, int id_aero1, int id_aero2)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Vuelo> lista = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, id_aerolinea, precio, fecha_salida, hora_salida, " +
                "id_aeropuerto_salida, fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, " +
                "id_piloto1, id_piloto2, id_tripulante1, id_tripulante2, id_tripulante3, tiempo_vuelo " +
                "FROM vuelos WHERE id_aeropuerto_salida = @a1 AND id_aeropuerto_llegada = @a2 " +
                "AND fecha_salida = CAST(@f1 AS timestamp) " +
                "AND fecha_llegada = CAST(@f2 AS timestamp) ", conexion);
            cmd.Parameters.AddWithValue("@f1", f1 + '%');
            cmd.Parameters.AddWithValue("@f2", f2 + '%');
            cmd.Parameters.AddWithValue("@a1", id_aero1);
            cmd.Parameters.AddWithValue("@a2", id_aero2);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga vuelos ordenados por precio
        /// </summary>
        /// <param name="f1"></param> filtro de fecha salida
        /// <param name="f2"></param> filtro de fecha regreso
        /// <param name="id_aero1"></param> filtro de aeropuerto de salida
        /// <param name="id_aero2"></param> filtro de aeropuerto de regreso
        /// <returns></returns> lista
        public List<Vuelo> CargarVuelosPrecio(string f1, string f2, int id_aero1, int id_aero2)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Vuelo> lista = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, id_aerolinea, precio, fecha_salida, hora_salida, " +
                "id_aeropuerto_salida, fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, " +
                "id_piloto1, id_piloto2, id_tripulante1, id_tripulante2, id_tripulante3, tiempo_vuelo " +
                "FROM vuelos WHERE id_aeropuerto_salida = @a1 AND id_aeropuerto_llegada = @a2 " +
                "AND fecha_salida = CAST(@f1 AS timestamp) " +
                "AND fecha_llegada = CAST(@f2 AS timestamp) " +
                "ORDER BY precio ", conexion);
            cmd.Parameters.AddWithValue("@f1", f1 + '%');
            cmd.Parameters.AddWithValue("@f2", f2 + '%');
            cmd.Parameters.AddWithValue("@a1", id_aero1);
            cmd.Parameters.AddWithValue("@a2", id_aero2);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga vuelos ordenados por tiempo
        /// </summary>
        /// <param name="f1"></param> filtro de fecha salida
        /// <param name="f2"></param> filtro de fecha regreso
        /// <param name="id_aero1"></param> filtro de aeropuerto salida
        /// <param name="id_aero2"></param> filtro de aeropuerto regreso
        /// <returns></returns> lista
        public List<Vuelo> CargarVuelosTiempo(string f1, string f2, int id_aero1, int id_aero2)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Vuelo> lista = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, id_aerolinea, precio, fecha_salida, hora_salida, " +
                "id_aeropuerto_salida, fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, " +
                "id_piloto1, id_piloto2, id_tripulante1, id_tripulante2, id_tripulante3, tiempo_vuelo " +
                "FROM vuelos WHERE id_aeropuerto_salida = @a1 AND id_aeropuerto_llegada = @a2 " +
                "AND fecha_salida = CAST(@f1 AS timestamp) " +
                "AND fecha_llegada = CAST(@f2 AS timestamp) " +
                "ORDER BY tiempo_vuelo ", conexion);
            cmd.Parameters.AddWithValue("@f1", f1 + '%');
            cmd.Parameters.AddWithValue("@f2", f2 + '%');
            cmd.Parameters.AddWithValue("@a1", id_aero1);
            cmd.Parameters.AddWithValue("@a2", id_aero2);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga vuelos para construir una escala
        /// </summary>
        /// <param name="f1"></param> filtro de fecha de salida
        /// <param name="f2"></param> filtro de fecha de regreso
        /// <param name="id_aero1"></param> filtro de aeropuerto de salida
        /// <param name="id_aero2"></param> filtro de aeropuerto de regreso
        /// <returns></returns> lista
        public List<Vuelo> CargarEscalasNormales(string f1, string f2, int id_aero1, int id_aero2)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Vuelo> lista = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, id_aerolinea, precio, fecha_salida, hora_salida, " +
                "id_aeropuerto_salida, fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, " +
                "id_piloto1, id_piloto2, id_tripulante1, id_tripulante2, id_tripulante3, tiempo_vuelo " +
                "FROM vuelos WHERE id_aeropuerto_salida = @a1 " +
                "AND fecha_salida = CAST(@f1 AS timestamp) " +
                "AND fecha_llegada = CAST(@f2 AS timestamp) " +
                "OR id_aeropuerto_llegada = @a2", conexion);
            cmd.Parameters.AddWithValue("@a1", id_aero1);
            cmd.Parameters.AddWithValue("@f1", f1 + '%');
            cmd.Parameters.AddWithValue("@f2", f2 + '%');
            cmd.Parameters.AddWithValue("@a2", id_aero2);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (lista.Count == 2)
                    {
                        return lista;
                    }
                    else
                    {
                        lista.Add(Cargar(dr));
                    }
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga vuelos para construir una escala
        /// </summary>
        /// <param name="f1"></param> filtro de fecha de salida
        /// <param name="f2"></param> filtro de fecha de regreso
        /// <param name="id_aero1"></param> filtro de aeropuerto de salida
        /// <param name="id_aero2"></param> filtro de aeropuerto de regreso
        /// <returns></returns> lista
        public Vuelo CargarEscala2(string f1, string f2, int id_aero1, int id_aero2)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Vuelo> lista = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, id_aerolinea, precio, fecha_salida, hora_salida, " +
                "id_aeropuerto_salida, fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, " +
                "id_piloto1, id_piloto2, id_tripulante1, id_tripulante2, id_tripulante3, tiempo_vuelo " +
                "FROM vuelos WHERE id_aeropuerto_salida = @a1 AND id_aeropuerto_llegada = @a2 " +
                "AND fecha_salida = CAST(@f1 AS timestamp) " +
                "AND fecha_llegada = CAST(@f2 AS timestamp) ", conexion);
            cmd.Parameters.AddWithValue("@a1", id_aero1);
            cmd.Parameters.AddWithValue("@a2", id_aero2);
            cmd.Parameters.AddWithValue("@f1", f1 + '%');
            cmd.Parameters.AddWithValue("@f2", f2 + '%');
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Vuelo vuelo = new Vuelo();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    vuelo = Cargar(dr);
                    conexion.Close();
                    return vuelo; 
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga vuelos inteligentes
        /// </summary>
        /// <param name="f1"></param> fecha de salida menos unos dias
        /// <param name="f2"></param> fecha de salida mas unos dias
        /// <param name="f3"></param> fecha de regreso menos unos dias
        /// <param name="f4"></param> fecha de regreso mas unos dias
        /// <param name="id_aero1"></param> aeropuerto de salida
        /// <param name="id_aero2"></param> aeropuerto de regreso
        /// <returns></returns> lista
        public List<Vuelo> CargarVuelosInteligentes(string f1, string f2, string f3, string f4, int id_aero1, int id_aero2)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Vuelo> lista = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, id_aerolinea, precio, fecha_salida, hora_salida, " +
                "id_aeropuerto_salida, fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, " +
                "id_piloto1, id_piloto2, id_tripulante1, id_tripulante2, id_tripulante3, tiempo_vuelo " +
                "FROM vuelos WHERE id_aeropuerto_salida = @a1 AND id_aeropuerto_llegada = @a2 " +
                "AND fecha_salida BETWEEN CAST(@f1 AS timestamp) AND CAST(@f2 AS timestamp) " +
                "AND fecha_llegada BETWEEN CAST(@f3 AS timestamp) AND CAST(@f4 AS timestamp) ORDER BY precio ", conexion);
            cmd.Parameters.AddWithValue("@f1", f1 + '%');
            cmd.Parameters.AddWithValue("@f2", f2 + '%');
            cmd.Parameters.AddWithValue("@f3", f3 + '%');
            cmd.Parameters.AddWithValue("@f4", f4 + '%');
            cmd.Parameters.AddWithValue("@a1", id_aero1);
            cmd.Parameters.AddWithValue("@a2", id_aero2);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (lista.Count == 5)
                    {
                        return lista;
                    }
                    else
                    {
                        lista.Add(Cargar(dr));
                    }

                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga reporte de vuelos segun aerolinea
        /// </summary>
        /// <param name="a"></param> aerolinea
        /// <returns></returns> lista
        public List<Vuelo> CargarReporte2(Aerolinea a)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Vuelo> lista = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, id_aerolinea, precio, fecha_salida, hora_salida, " +
                " id_aeropuerto_salida, fecha_llegada, hora_llegada, id_aeropuerto_llegada, id_avion, " +
                " id_piloto1, id_piloto2, id_tripulante1, id_tripulante2, id_tripulante3, tiempo_vuelo " +
                " FROM public.vuelos WHERE id_aerolinea = @id", conexion);
            cmd.Parameters.AddWithValue("@id", a.id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }
    }
}

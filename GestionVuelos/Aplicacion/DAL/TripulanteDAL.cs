﻿using Aplicacion.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.DAL
{
    class TripulanteDAL
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Inserta a la BD
        /// </summary>
        /// <param name="t"></param> tripulante
        public void InsertarDatos(Tripulante t)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO tripulantes(cedula, nombre, fecha_nacimiento, " +
                " id_aerolinea, rol, estado) VALUES ('" + t.cedula + 
                "', '" + t.nombre + "', '" + t.fechaNacimiento + "', '" + t.aerolinea.id + 
                "', '" + t.rol + "', '" + t.estado + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Cambia el estado de disponibilidad de un tripulante
        /// </summary>
        /// <param name="t"></param> tripulante
        public void CambiarEstado(Tripulante t)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("UPDATE tripulantes SET estado = @estado WHERE id = @id", conexion);
            cmd.Parameters.AddWithValue("@estado", t.estado);
            cmd.Parameters.AddWithValue("@id", t.id);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Carga una lista de tripulantes
        /// </summary>
        /// <returns></returns> lista
        public List<Tripulante> ConsultarDatos()
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Tripulante> lista = new List<Tripulante>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, fecha_nacimiento, id_aerolinea, " +
                " rol, estado FROM public.tripulantes ORDER BY id_aerolinea", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Crea un tripulante con los datos de la BD
        /// </summary>
        /// <param name="dr"></param> datos 
        /// <returns></returns> tripulante
        private Tripulante Cargar(NpgsqlDataReader dr)
        {
            Tripulante t = new Tripulante()
            {
                id = dr.GetInt32(0),
                cedula = dr.GetString(1),
                nombre = dr.GetString(2),
                fechaNacimiento = dr.GetDateTime(3),
                aerolinea = new AerolineaDAL().CargarAT(dr.GetInt32(0)),
                rol = dr.GetString(5),
                estado = dr.GetBoolean(6),
                
            };
            return t;
        }

        /// <summary>
        /// Carga un tripulante de tipo piloto una aerolinea 
        /// </summary>
        /// <param name="id"></param> filtro de aerolinea
        /// <returns></returns> tripulante
        public Tripulante CargarP1(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, a.cedula, a.nombre, a.fecha_nacimiento, " +
                " a.id_aerolinea, a.rol, a.estado FROM public.tripulantes as a " +
                " inner join vuelos as v on v.id_piloto1 = a.id " +
                " WHERE v.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Tripulante tripulante = new Tripulante();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    tripulante = Cargar(dr);
                    conexion.Close();
                    return tripulante;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga un tripulante de tipo piloto una aerolinea 
        /// </summary>
        /// <param name="id"></param> filtro de aerolinea
        /// <returns></returns> tripulante
        public Tripulante CargarP2(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, a.cedula, a.nombre, a.fecha_nacimiento, " +
                " a.id_aerolinea, a.rol, a.estado FROM public.tripulantes as a " +
                " inner join vuelos as v on v.id_piloto2 = a.id " +
                " WHERE v.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Tripulante tripulante = new Tripulante();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    tripulante = Cargar(dr);
                    conexion.Close();
                    return tripulante;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga un tripulante de tipo servicio al cliente una aerolinea 
        /// </summary>
        /// <param name="id"></param> filtro de aerolinea
        /// <returns></returns> tripulante
        public Tripulante CargarT1(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, a.cedula, a.nombre, a.fecha_nacimiento, " +
                " a.id_aerolinea, a.rol, a.estado FROM public.tripulantes as a " +
                " inner join vuelos as v on v.id_tripulante1 = a.id " +
                " WHERE v.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Tripulante tripulante = new Tripulante();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    tripulante = Cargar(dr);
                    conexion.Close();
                    return tripulante;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga un tripulante de tipo servicio al cliente una aerolinea 
        /// </summary>
        /// <param name="id"></param> filtro de aerolinea
        /// <returns></returns> tripulante
        public Tripulante CargarT2(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, a.cedula, a.nombre, a.fecha_nacimiento, " +
                " a.id_aerolinea, a.rol, a.estado FROM public.tripulantes as a " +
                " inner join vuelos as v on v.id_tripulante2 = a.id " +
                " WHERE v.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Tripulante tripulante = new Tripulante();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    tripulante = Cargar(dr);
                    conexion.Close();
                    return tripulante;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga un tripulante de tipo servicio al cliente una aerolinea 
        /// </summary>
        /// <param name="id"></param> filtro de aerolinea
        /// <returns></returns> tripulante
        public Tripulante CargarT3(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, a.cedula, a.nombre, a.fecha_nacimiento, " +
                " a.id_aerolinea, a.rol, a.estado FROM public.tripulantes as a " +
                " inner join vuelos as v on v.id_tripulante3 = a.id " +
                " WHERE v.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Tripulante tripulante = new Tripulante();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    tripulante = Cargar(dr);
                    conexion.Close();
                    return tripulante;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga lista de pilotos de una aerolinea
        /// </summary>
        /// <param name="id"></param> filtro de aerolinea
        /// <returns></returns> lista
        public List<Tripulante> CargarPilotos(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Tripulante> lista = new List<Tripulante>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, fecha_nacimiento, id_aerolinea, " +
                " rol, estado FROM tripulantes WHERE estado = true and rol = 'Piloto' and id_aerolinea = @id" , conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (lista.Count == 2)
                    {
                        return lista;
                    }
                    else
                    {
                        lista.Add(Cargar(dr));
                    }
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga lista de servicio al cliente de una aerolinea
        /// </summary>
        /// <param name="id"></param> filtro de aerolinea
        /// <returns></returns> lista
        public List<Tripulante> CargarTripulantes(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Tripulante> lista = new List<Tripulante>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, fecha_nacimiento, id_aerolinea, " +
                " rol, estado FROM tripulantes WHERE estado = true and rol = 'Servicio al Cliente' and id_aerolinea = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (lista.Count == 3)
                    {
                        return lista;
                    }
                    else
                    {
                        lista.Add(Cargar(dr));
                    }
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga tripulantes de una aerolinea para un reporte
        /// </summary>
        /// <param name="id"></param> filtro de aerolinea
        /// <returns></returns> lista
        public List<Tripulante> CargarReporte4(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Tripulante> lista = new List<Tripulante>();
            NpgsqlCommand cmd = new NpgsqlCommand("select t.id, cedula, t.nombre, fecha_nacimiento," +
                " id_aerolinea, rol, estado from tripulantes t " +
                "inner join aerolineas a on a.id = t.id_aerolinea where a.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

    }
}  

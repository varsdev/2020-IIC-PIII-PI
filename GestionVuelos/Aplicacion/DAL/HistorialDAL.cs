﻿using Aplicacion.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.DAL
{
    class HistorialDAL
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Inserta a la BD
        /// </summary>
        /// <param name="h"></param> historial
        public void InsertarDatos(Historial h)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO historiales (cedula, nombre_pais_salida, nombre_pais_llegada," +
                " nombre_pais_escala1, nombre_pais_escala2, fecha_compra_vuelo, hora_compra_vuelo, tiempo_vuelo," +
                " costo_total) VALUES ('" + h.cedula + "', '" + h.nombrePaisSalida + "', '" + h.nombrePaisLlegada +
                "', '" + h.nombrePaisEscala1 + "', '" + h.nombrePaisEscala2 + "', '" + h.fechaCompraVuelo +
                "', '" + h.horaCompraVuelo + "', '" + h.tiempoVuelo + "', '" + h.costoTotal + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Carga lista de historiales de un pasajero
        /// </summary>
        /// <param name="ced"></param> filtro de cedula
        /// <returns></returns> lista
        public List<Historial> ConsultarDatos(string ced)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Historial> lista = new List<Historial>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre_pais_salida, nombre_pais_llegada," +
                " nombre_pais_escala1, nombre_pais_escala2, fecha_compra_vuelo, hora_compra_vuelo," +
                " tiempo_vuelo, costo_total FROM historiales WHERE cedula = @ced ", conexion);
            cmd.Parameters.AddWithValue("@ced", ced);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Crea un historial con los datos de la BD
        /// </summary>
        /// <param name="dr"></param> datos
        /// <returns></returns> historial
        private Historial Cargar(NpgsqlDataReader dr)
        {
            Historial h = new Historial()
            {
                id = dr.GetInt32(0),
                cedula = dr.GetString(1),
                nombrePaisSalida = dr.GetString(2),
                nombrePaisLlegada = dr.GetString(3),
                nombrePaisEscala1 = dr.GetString(4),
                nombrePaisEscala2 = dr.GetString(5),
                fechaCompraVuelo = dr.GetDateTime(6),
                horaCompraVuelo = dr.GetTimeSpan(7),
                tiempoVuelo = dr.GetTimeSpan(8),
                costoTotal = dr.GetDouble(9),
            };
            return h;
        }

    }
}

﻿using Aplicacion.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.DAL
{
    class AeropuertoDAL
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Inserta a la BD
        /// </summary>
        /// <param name="a"></param> aeropuerto
        public void InsertarDatos(Aeropuerto a)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO public.aeropuertos(iata, nombre, pais) VALUES ('" + a.iata + "', '" + a.nombre + "', '" + a.pais + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Carga lista de aeropuertos
        /// </summary>
        /// <returns></returns> una lista
        public List<Aeropuerto> ConsultarDatos()
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Aeropuerto> lista = new List<Aeropuerto>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, iata, nombre, pais FROM public.aeropuertos ", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();

            return lista;
        }

        /// <summary>
        /// Crea un aeropuerto con los datos de la BD
        /// </summary>
        /// <param name="dr"></param> datos 
        /// <returns></returns> aeropuerto
        private Aeropuerto Cargar(NpgsqlDataReader dr)
        {
            Aeropuerto a = new Aeropuerto()
            {
                id = dr.GetInt32(0),
                iata = dr.GetString(1),
                nombre = dr.GetString(2),
                pais = dr.GetString(3),
            };
            return a;
        }

        /// <summary>
        /// Carga un aeropuerto
        /// </summary>
        /// <param name="id"></param> filtro de id
        /// <returns></returns> aeropuerto
        public Aeropuerto CargarAeV1(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, iata, nombre, pais FROM public.aeropuertos a " +
                " inner join vuelos v on v.id_aeropuerto_salida = a.id " +
                " WHERE v.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Aeropuerto aeropuerto = new Aeropuerto();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    aeropuerto = Cargar(dr);
                    conexion.Close();
                    return aeropuerto;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga un aeropuerto
        /// </summary>
        /// <param name="id"></param> filtro de id
        /// <returns></returns> aeropuerto
        public Aeropuerto CargarAeV2(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, iata, nombre, pais FROM public.aeropuertos a " +
                " inner join vuelos v on v.id_aeropuerto_llegada = a.id " +
                " WHERE v.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Aeropuerto aeropuerto = new Aeropuerto();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    aeropuerto = Cargar(dr);
                    conexion.Close();
                    return aeropuerto;
                }
            }
            conexion.Close();
            return null;
        }
    }
}

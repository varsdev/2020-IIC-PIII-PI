﻿using Aplicacion.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.DAL
{
    class AvionDAL
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Inserta a la BD
        /// </summary>
        /// <param name="a"></param> avion
        public void InsertarDatos(Avion a)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO public.aviones( modelo, anno_construccion, id_aerolinea, capacidad, estado) VALUES ('" + a.modelo +
                "', '" + a.annoConstruccion + "', '" + a.aerolinea.id + "', '" + a.capacidad +
                "', '" + a.estado + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Carga lista de aviones
        /// </summary>
        /// <returns></returns> una lista
        public List<Avion> ConsultarDatos()
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Avion> lista = new List<Avion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, modelo, anno_construccion, id_aerolinea, capacidad, estado FROM public.aviones ORDER BY id_aerolinea;", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Crea un avion con los datos de la BD
        /// </summary>
        /// <param name="dr"></param> datos
        /// <returns></returns> avion
        private Avion Cargar(NpgsqlDataReader dr)
        {
            Avion a = new Avion()
            {
                id = dr.GetInt32(0),
                modelo = dr.GetString(1),
                annoConstruccion = dr.GetString(2),
                aerolinea = new AerolineaDAL().CargarAA(dr.GetInt32(0)),
                capacidad = dr.GetString(4),
                estado = dr.GetBoolean(5),

            };
            return a;
        }

        /// <summary>
        /// Carga un avion especifico
        /// </summary>
        /// <param name="id"></param> filtro de id
        /// <returns></returns> avion
        public Avion CargarAvionVuelo(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, modelo, anno_construccion, id_aerolinea, capacidad, estado " +
                " FROM aviones WHERE estado = true and id_aerolinea = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Avion avion = new Avion();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    avion = Cargar(dr);
                    conexion.Close();
                    return avion;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Cambia el estado de disponibilidad de un avion
        /// </summary>
        /// <param name="a"></param> avion
        public void CambiarEstado(Avion a)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("UPDATE aviones SET estado = @estado WHERE id = @id", conexion);
            cmd.Parameters.AddWithValue("@estado", a.estado);
            cmd.Parameters.AddWithValue("@id", a.id);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Carga un avion especifico
        /// </summary>
        /// <param name="id"></param> filtro de id
        /// <returns></returns> avion
        public Avion CargarAvV(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, modelo, anno_construccion, " +
                " a.id_aerolinea, capacidad, estado FROM public.aviones a " +
                " right join vuelos v on v.id_avion = a.id " +
                " WHERE v.id = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Avion avion = new Avion();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    avion = Cargar(dr);
                    conexion.Close();
                    return avion;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga una lista de aviones de una aerolinea
        /// </summary>
        /// <param name="id"></param> filtro de id
        /// <returns></returns> avion
        public List<Avion> CargarAvAe(int id)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<Avion> lista = new List<Avion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT a.id, modelo, anno_construccion, " +
                " a.id_aerolinea, capacidad, estado FROM public.aviones a " +
                " right join aerolineas e on e.id = a.id_aerolinea WHERE a.id_aerolinea = @id", conexion);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga un reporte de los aviones
        /// </summary>
        /// <returns></returns> reporte
        public List<object> CargarReporte3()
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<object> lista = new List<object>();
            NpgsqlCommand cmd = new NpgsqlCommand("select a.id, modelo, anno_construccion, a.id_aerolinea," +
                " capacidad, estado, count(DISTINCT a.id) from aviones a inner join " +
                "vuelos b on b.id_avion = a.id group by a.id", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Avion a = new Avion();
                    a.id = dr.GetInt32(0);
                    a.modelo = dr.GetString(1);
                    a.annoConstruccion = dr.GetString(2);
                    a.aerolinea = new AerolineaDAL().CargarAA(dr.GetInt32(0));
                    a.capacidad = dr.GetString(4);
                    a.estado = dr.GetBoolean(5);
                    int cant = dr.GetInt32(6);
                    lista.Add("Modelo: " + a.modelo + ", Año de construcción: " + a.annoConstruccion +
                        ", Aerolinea: "+a.aerolinea+ ", Capacidad: " +a.capacidad + ", Estado del Avión: " +
                        (a.estado==true?"Activo":"Inactivo") +", Cantidad de vuelos: " + cant);
                }
            }
            conexion.Close();
            return lista;
        }
    }
}

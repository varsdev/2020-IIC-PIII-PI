﻿using Aplicacion.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.DAL
{
    class UsuarioDAL
    {
        private NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Valida si el usuario digitado puede hacer login
        /// </summary>
        /// <param name="u"></param> usuario
        /// <returns></returns> usuario
        public Usuario Autenticar(Usuario u)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, edad, contrasenna, tipo FROM usuarios WHERE cedula = '" + u.cedula + "' and contrasenna = '" + u.contrasenna + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Usuario usuario = new Usuario();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    usuario = Cargar(dr);
                    conexion.Close();
                    return usuario;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga un usuario
        /// </summary>
        /// <param name="cedula"></param> filtro de cedula 
        /// <returns></returns> usuario
        public Usuario CargarUsuario(string cedula)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, edad, contrasenna, tipo FROM usuarios WHERE cedula = '" + cedula + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            Usuario usuario = new Usuario();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    usuario = Cargar(dr);
                    conexion.Close();
                    return usuario;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Crea un usuario con los datos de la BD
        /// </summary>
        /// <param name="dr"></param> datos 
        /// <returns></returns> usuario
        private Usuario Cargar(NpgsqlDataReader dr)
        {
            Usuario u = new Usuario()
            {
                id = dr.GetInt32(0),
                cedula = dr.GetString(1),
                nombre = dr.GetString(2),
                edad = dr.GetInt32(3),
                contrasenna = dr.GetString(4),
                tipo = dr.GetString(5)

            };
            return u;
        }
        
        /// <summary>
        /// Inserta a la BD
        /// </summary>
        /// <param name="u"></param> usuario
        public void InsertarDatos(Usuario u)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO usuarios (cedula, nombre, edad, contrasenna, tipo) VALUES ('" + u.cedula + "', '" + u.nombre + "', '" + u.edad + "', '" + u.contrasenna + "', '" + u.tipo +"')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

    }
}

﻿using Aplicacion.DAL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.BOL
{
    class AeropuertoBOL
    {
        /// <summary>
        /// Valida un aeropuerto
        /// </summary>
        /// <param name="a"></param> aeropuerto
        private void Validar(Aeropuerto a)
        {
            if (string.IsNullOrEmpty(a.nombre))
            {
                throw new Exception("Nombre requerido");
            }
            if (string.IsNullOrEmpty(a.iata))
            {
                throw new Exception("IATA requerido");
            }
            if (string.IsNullOrEmpty(a.pais))
            {
                throw new Exception("País requerido");
            }
        }

        /// <summary>
        /// Inserta un aeropuerto a la BD
        /// </summary>
        /// <param name="a"></param> aeropuerto
        public void Insertar(Aeropuerto a)
        {
            Validar(a);
            if (a != null)
            {
                new AeropuertoDAL().InsertarDatos(a);
            }
        }

        /// <summary>
        /// Carga una lista de aeropuertos
        /// </summary>
        /// <returns></returns> una lista
        public List<Aeropuerto> CargarAeropuertos()
        {
            return new AeropuertoDAL().ConsultarDatos();
        }
    }
}

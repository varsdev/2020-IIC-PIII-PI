﻿using Aplicacion.DAL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.BOL
{
    class TripulanteBOL
    {

        /// <summary>
        /// Valida un tripulante
        /// </summary>
        /// <param name="t"></param> tripulante 
        private void Validar(Tripulante t)
        {
            
            if (string.IsNullOrEmpty(t.cedula))
            {
                throw new Exception("Cedula requerido");
            }
            if (string.IsNullOrEmpty(t.nombre))
            {
                throw new Exception("Nombre requerido");
            }
            if (t.fechaNacimiento==null)
            {
                throw new Exception("Fecha de Nacimiento requerido");
            }

        }

        /// <summary>
        /// Inserta una tripulante a la BD
        /// </summary>
        /// <param name="t"></param> tripulante
        public void Insertar(Tripulante t)
        {
            Validar(t);
            if (t != null)
            {
                new TripulanteDAL().InsertarDatos(t);
            }
        }

        /// <summary>
        /// Carga una lista de tripulantes
        /// </summary>
        /// <returns></returns> una lista
        public List<Tripulante> CargarTripulantes()
        {
            return new TripulanteDAL().ConsultarDatos();
        }

        /// <summary>
        /// Carga una lista de tripulantes para un reporte
        /// </summary>
        /// <param name="id"></param> filtro 
        /// <returns></returns> una lista 
        public List<Tripulante> CargarReportes4(int id)
        {
            return new TripulanteDAL().CargarReporte4(id);
        }
    }
}

﻿using Aplicacion.DAL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.BOL
{
    class HistorialBOL
    {
        /// <summary>
        /// Inserta un historial a la BD
        /// </summary>
        /// <param name="h"></param>
        public void Insertar(Historial h)
        {
            if (h != null)
            {
                new HistorialDAL().InsertarDatos(h);
            }
        }

        /// <summary>
        /// Carga lista de historiales de una persona
        /// </summary>
        /// <param name="cedula"></param> filtro
        /// <returns></returns> una lista
        public List<Historial> CargarHistorial(string cedula)
        {
            return new HistorialDAL().ConsultarDatos(cedula);
        }

    }
}

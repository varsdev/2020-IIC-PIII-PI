﻿using Aplicacion.DAL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.BOL
{
    class UsuarioBOL
    {
        /// <summary>
        /// Valida el usuario para iniciar sesion
        /// </summary>
        /// <param name="u"></param> usuario
        /// <returns></returns> un usuario
        public Usuario Login(Usuario u)
        {
            if (u != null)
            {
                u.contrasenna = GetMD5(u.contrasenna);
                return new UsuarioDAL().Autenticar(u);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Debe crear un usuario");
                return null;
            }
        }

        /// <summary>
        /// Carga un usuario 
        /// </summary>
        /// <param name="cedula"></param> filtro
        /// <returns></returns> usuario
        public Usuario CargarUsuario(string cedula)
        {
            if (cedula != "")
            {
                return new UsuarioDAL().CargarUsuario(cedula);
            }
            return null;

        }

        /// <summary>
        /// Inserta un usuario a la BD
        /// </summary>
        /// <param name="u"></param> usuario
        public void Insertar(Usuario u)
        {
            if (u != null)
            {
                u.contrasenna = GetMD5(u.contrasenna);
                new UsuarioDAL().InsertarDatos(u);
            }
        }

        /// <summary>
        /// Encripta un string de contrasenna
        /// </summary>
        /// <param name="str"></param> contrasenna a encriptar
        /// <returns></returns> string encriptado
        public string GetMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

    }
}

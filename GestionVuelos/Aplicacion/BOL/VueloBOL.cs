﻿using Aplicacion.DAL;
using Aplicacion.Entities;
using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.BOL
{
    class VueloBOL
    {
        /// <summary>
        /// Valida un vuelo
        /// </summary>
        /// <param name="v"></param> vuelo
        private void Validar(Vuelo v)
        {

            if (v.precio==0)
            {
                throw new Exception("Precio requerido");
            }

        }

        /// <summary>
        /// Inserta un vuelo a la BD
        /// </summary>
        /// <param name="v"></param>
        public void Insertar(Vuelo v)
        {
            Validar(v);
            if (v != null)
            {
                new VueloDAL().InsertarDatos(v);
            }
        }

        /// <summary>
        /// Carga lista de vuelos
        /// </summary>
        /// <returns></returns> una lista
        public List<Vuelo> CargarVuelo()
        {
            return new VueloDAL().ConsultarDatos();
        }

        /// <summary>
        /// Carga lista de vuelos para reporte
        /// </summary>
        /// <param name="f1"></param> filtro de fecha 1
        /// <param name="f2"></param> filtro de fecha 2
        /// <returns></returns> una lista 
        public List<Vuelo> CargarReportes(string f1,string f2)
        {
            return new VueloDAL().CargarReporte(f1,f2);
        }

        /// <summary>
        /// Carga lista de vuelos para reporte
        /// </summary>
        /// <param name="a"></param> filtro de aerolinea
        /// <returns></returns> una lista
        public List<Vuelo> CargarReportes2(Aerolinea a)
        {
            return new VueloDAL().CargarReporte2(a);
        }

        /// <summary>
        /// Carga lista de vuelos para comprar
        /// </summary>
        /// <param name="f1"></param> filtro de fecha salida
        /// <param name="f2"></param> filtro de fecha regreso
        /// <param name="a1"></param> filtro de aeropuerto salida
        /// <param name="a2"></param> filtro de aeropuerto regreso
        /// <returns></returns> una lista
        public List<Vuelo> CargarVuelosPasajeros(string f1, string f2, int a1, int a2)
        {
            return new VueloDAL().CargarVuelosNormal(f1, f2, a1, a2);
        }

        /// <summary>
        /// Carga lista de vuelos para comprar ordenados por precio
        /// </summary>
        /// <param name="f1"></param> filtro de fecha salida
        /// <param name="f2"></param> filtro de fecha regreso
        /// <param name="a1"></param> filtro de aeropuerto salida
        /// <param name="a2"></param> filtro de aeropuerto regreso
        /// <returns></returns> una lista
        public List<Vuelo> CargarVuelosPrecio(string f1, string f2, int a1, int a2)
        {
            return new VueloDAL().CargarVuelosPrecio(f1, f2, a1, a2);
        }

        /// <summary>
        /// Carga lista de vuelos para comprar ordenados por tiempo
        /// </summary>
        /// <param name="f1"></param> filtro de fecha salida
        /// <param name="f2"></param> filtro de fecha regreso
        /// <param name="a1"></param> filtro de aeropuerto salida
        /// <param name="a2"></param> filtro de aeropuerto regreso
        /// <returns></returns> una lista
        public List<Vuelo> CargarVuelosTiempo(string f1, string f2, int a1, int a2)
        {
            return new VueloDAL().CargarVuelosTiempo(f1, f2, a1, a2);
        }

        /// <summary>
        /// Carga lista de vuelos inteligentes
        /// </summary>
        /// <param name="f1"></param> filtro de fecha salida menos unos dias 
        /// <param name="f2"></param> filtro de fecha salida mas unos dias
        /// <param name="f3"></param> filtro de fecha regreso menos unos dias
        /// <param name="f4"></param> filtro de fecha regreso mas unos dias
        /// <param name="a1"></param> filtro de aeropuerto salida
        /// <param name="a2"></param> filtro de aeropuerto regreso 
        /// <returns></returns>
        public List<Vuelo> CargarVuelosInteligentes(string f1, string f2, string f3, string f4, int a1, int a2)
        {
            return new VueloDAL().CargarVuelosInteligentes(f1, f2, f3, f4, a1, a2);
        }

        /// <summary>
        /// Carga lista de escalas
        /// </summary>
        /// <param name="f1"></param> filtro de fecha de salida
        /// <param name="f2"></param> filtro de fecha de regreso
        /// <param name="id1"></param> filtro de aeropuerto salida
        /// <param name="id2"></param> filtro de aeropuerto llegada
        /// <returns></returns> una lista
        public List<Vuelo> CargarEscalasNormales(string f1, string f2, int id1, int id2)
        {
            List<Vuelo> Escalas = new List<Vuelo>();
            Vuelo v = new Vuelo();
            int cont = 0;

            foreach (Vuelo item in new VueloDAL().CargarEscalasNormales(f1, f2, id1, id2))
            {
                if (cont == 0)
                {
                    v = item;
                    Escalas.Add(v);
                }
              
                if (cont > 0)
                {
                    if (v.aeropuertoLlegada.nombre == item.aeropuertoSalida.nombre)
                    {
                        Escalas.Add(item);
                    }
                }
                cont++;
            }
            return Escalas;
        }

        /// <summary>
        /// Carga lista de escalas
        /// </summary>
        /// <param name="f1"></param> filtro de fecha de salida
        /// <param name="f2"></param> filtro de fecha de regreso
        /// <param name="id1"></param> filtro de aeropuerto salida
        /// <param name="id2"></param> filtro de aeropuerto llegada
        /// <returns></returns> una lista
        public List<Vuelo> CargarEscalasDobles(string f1, string f2, int id1, int id2)
        {
            List<Vuelo> Escalas = new List<Vuelo>();
            Vuelo v = new Vuelo();
            Vuelo v2 = new Vuelo();
            Vuelo v3 = new Vuelo();

            int cont = 0;

            foreach (Vuelo item in new VueloDAL().CargarEscalasNormales(f1, f2, id1, id2))
            {
                if (cont == 0)
                {
                    v = item;
                    Escalas.Add(v);
                }
                if (cont == 1)
                {
                    v2 = item;                   
                    v3 = new VueloDAL().CargarEscala2(f1, f2, v.aeropuertoLlegada.id, v2.aeropuertoSalida.id);
                    Escalas.Add(v3);
                    Escalas.Add(v2);
                }
                cont++;
            }
            return Escalas;
        }
    }
}

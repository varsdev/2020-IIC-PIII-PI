﻿using Aplicacion.DAL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.BOL
{
    class AvionBOL
    {

        /// <summary>
        /// Valida un avion
        /// </summary>
        /// <param name="a"></param> avion
        private void Validar(Avion a)
        {

            if (string.IsNullOrEmpty(a.modelo))
            {
                throw new Exception("Cedula requerido");
            }
            if (a.annoConstruccion == null)
            {
                throw new Exception("Año de construcción requerido");
            }
            if (string.IsNullOrEmpty(a.capacidad))
            {
                throw new Exception("capacidad requerido");
            }

        }

        /// <summary>
        /// Inserta un avion a la BD
        /// </summary>
        /// <param name="a"></param> avion
        public void Insertar(Avion a)
        {
            Validar(a);
            if (a != null)
            {
                new AvionDAL().InsertarDatos(a);
            }
        }

        /// <summary>
        /// Carga lista de aviones
        /// </summary>
        /// <returns></returns> una lista
        public List<Avion> CargarAviones()
        {
            return new AvionDAL().ConsultarDatos();
        }

        /// <summary>
        /// Carga lista de aviones especificos
        /// </summary>
        /// <param name="id"></param> filtro 
        /// <returns></returns> una lista
        public List<Avion> CargarAvAe(int id) 
        {
            return new AvionDAL().CargarAvAe(id);
        }

        /// <summary>
        /// Carga reporte de aviones
        /// </summary>
        /// <returns></returns> una lista
        public List<object> CargarReportes3()
        {
            return new AvionDAL().CargarReporte3();
        }
    }
}

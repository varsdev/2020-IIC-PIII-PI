﻿using Aplicacion.DAL;
using Aplicacion.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.BOL
{
    class AerolineaBOL
    {
        /// <summary>
        /// Valida una aerolinea
        /// </summary>
        /// <param name="a"></param> aerolinea
        private void Validar(Aerolinea a)
        {
            if (string.IsNullOrEmpty(a.nombre))
            {
                throw new Exception("Nombre requerido");
            }
            if (string.IsNullOrEmpty(a.annoFundacion))
            {
                throw new Exception("Año de fundación requerido");
            }
            if (string.IsNullOrEmpty(a.tipo))
            {
                throw new Exception("Tipo requerido");
            }
        }

        /// <summary>
        /// Inserta una aerolinea a la BD
        /// </summary>
        /// <param name="a"></param> aerolinea
        public void Insertar(Aerolinea a)
        {
            Validar(a);
            if (a != null)
            {
                new AerolineaDAL().InsertarDatos(a);
            }
        }

        /// <summary>
        /// Carga lista de aerolineas
        /// </summary>
        /// <returns></returns> una lista
        public List<Aerolinea> CargarAerolineas()
        {
            return new AerolineaDAL().ConsultarDatos();
        }
    }
}

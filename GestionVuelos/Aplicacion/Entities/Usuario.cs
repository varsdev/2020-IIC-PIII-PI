﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Entities
{
    class Usuario
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }
        public int edad { get; set; }
        public string contrasenna { get; set; }
        public string tipo { get; set; }

        public override string ToString()
        {
            return "" + id +" "+ cedula + " " + nombre + " " + edad + " " + 
                contrasenna + " " + tipo ;
        }
    }
}

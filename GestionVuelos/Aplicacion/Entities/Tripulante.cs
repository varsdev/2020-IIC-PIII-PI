﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Entities
{
    class Tripulante
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public Aerolinea aerolinea { get; set; }
        public string rol { get; set; }
        public bool estado { get; set; }

        public override string ToString()
        {
            return "" + id + " " + cedula + " " + nombre +" " + fechaNacimiento + " " +
                aerolinea.nombre + " " + rol + " " + estado;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Entities
{
    class Aerolinea
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string annoFundacion { get; set; }
        public string tipo { get; set; }


        public override string ToString()
        {
            return "" + nombre ;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Entities
{
    class Historial
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombrePaisSalida { get; set; }
        public string nombrePaisLlegada { get; set; }
        public string nombrePaisEscala1 { get; set; }
        public string nombrePaisEscala2 { get; set; }
        public DateTime fechaCompraVuelo { get; set; }
        public TimeSpan horaCompraVuelo { get; set; }
        public TimeSpan tiempoVuelo { get; set; }
        public double costoTotal { get; set; }

        public override string ToString()
        {
            return "" + id + " " + cedula + " " + nombrePaisSalida + " " + nombrePaisLlegada + " " + nombrePaisEscala1 
                + " " + nombrePaisEscala2 + " " + fechaCompraVuelo.ToShortDateString() + " " + horaCompraVuelo
                + " " + tiempoVuelo + " " + costoTotal;
        }

    }
}

﻿using Npgsql.TypeHandlers.DateTimeHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Entities
{
    class Vuelo
    {
        public int id { get; set; }
        public Aerolinea aerolinea { get; set; }
        public double precio { get; set; }
        public DateTime fechaSalida { get; set; }
        public TimeSpan horaSalida { get; set; }
        public Aeropuerto aeropuertoSalida { get; set; }
        public DateTime fechaLlegada { get; set; }
        public TimeSpan horaLlegada { get; set; }
        public Aeropuerto aeropuertoLlegada { get; set; }
        public Avion avion { get; set; }
        public Tripulante piloto1 { get; set; }
        public Tripulante piloto2 { get; set; }
        public Tripulante tripulante1 { get; set; }
        public Tripulante tripulante2 { get; set; }
        public Tripulante tripulante3 { get; set; }
        public TimeSpan tiempoVuelo { get; set; }
        
        public override string ToString()
        {
            return "" + id + " " + aerolinea + " " + precio + " " + fechaSalida.ToLongDateString() + " " + " " + horaSalida + " " +
                aeropuertoSalida.iata + " " + fechaLlegada.ToLongDateString() + " " + " " + horaLlegada + " " + aeropuertoLlegada.iata
                + " " + avion + " " + piloto1.nombre + " " + piloto2.nombre + " " + 
                tripulante1.nombre + " " + tripulante2.nombre + " " + tripulante3.nombre + " " + tiempoVuelo;
        }

    }
}

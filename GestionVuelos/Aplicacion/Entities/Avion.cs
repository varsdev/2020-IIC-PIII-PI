﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Entities
{
    class Avion
    {
        public int id { get; set; }
        public string modelo { get; set; }
        public string annoConstruccion { get; set; }
        public Aerolinea aerolinea { get; set; }
        public string capacidad { get; set; }
        public bool estado { get; set; }

        public override string ToString()
        {
            return modelo; 
        }
    }
}
